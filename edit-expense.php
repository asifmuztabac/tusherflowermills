<?php
	$title = "জমা খরচের হিসাব";
	if (!isset($_GET['expense'])) {
		header("Location: expense.php");
		exit;
	}
	require_once "includes/header.php";

	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<h4 class="block-title">Edit Account Name</h4>
					<?php 
						$validator = new Validate(new ErrorHandler);
						if (!empty($_POST)) {
							$validator->check($_POST, [
								'name' => [
									'required' => true
								],
								'vou_no' => [
									'required' => true
								],
								'amount' => [
									'required' => true
								],
								'date' => [
									'required' => true
								]

							]);

							if($validator->passed()) {
								$expense = new Expense;

								$add = $expense->update([
									'name' => $_POST['name'],
									'vouchar_no' => $_POST['vou_no'],
									'amount' => $_POST['amount'],
									'date' => $_POST['date'],
							'details' => $_POST['details']], ['id', '=', $_GET['expense']]);

								if ($add) {
									echo '<p class="alert alert-success fade in">Data update successfully<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></p>';
								}else{
									echo '<p class="alert alert-danger fade in">There was problem updating data<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></p>';
								}


							}

						}

						$expenseHelper = new Expense;
						$account = new AccountName;
						$data = $expenseHelper->firstData($_GET['expense']);
						if(!empty($data)) :
					?>

					<div class="row">
						<div class="col-md-6 col-md-offset-3 box-style">
							<form action="" method="post">
								<div class="form-group">
									<label for="name" class="control-label">Expenditures<span class="star">*</span></label>
									<select required="" name="name" id="name" class="form-control">
										<option value="">Select one..</option>
										<?php if (!empty($account->get())) : foreach ($account->get() as $em) :?>
										<option value="<?=$em->name?>"><?=$em->name;?></option>	
										<?php endforeach; endif;?>
									</select>
								</div>
								<div class="form-group">
									<label for="vou_no" class="control-label">Vouchar No <span class="star">*</span></label>
									<input type="text" required="" value="<?=$data->vouchar_no;?>" name="vou_no" class="form-control" id="vou_no" placeholder="Enter Vouchar No here">
								</div>								
								<div class="form-group">
									<label for="amount" class="control-label">Amount <span class="star">*</span></label>
									<input type="text" required="" value="<?=$data->amount;?>" name="amount" id="amount" class="form-control">
								</div>
								<div class="form-group">
									<label for="adding_date" class="control-label">Date <span class="star">*</span></label>
									<input type="text" readonly="true" value="<?=$data->date;?>" value="<?php echo date('Y-m-d');?>" class="form-control" id="adding_date" name="date">
								</div>
								<div class="form-group">
									<label for="details" class="control-label">Details</label>
									<textarea name="details" id="details" rows="5" placeholder="Enter data here" class="form-control"><?=$data->details;?></textarea>
								</div>								
								<input type="submit" class="btn custom-btn" value="Submit">
							</form>
						</div>
					</div>

					<?php else : ?>
						<h3>Data Not Found!</h3>
					<?php endif;?>
				</div>
			</div>
		</div>

		<script src="js/bootstrap-datepicker.js"></script>
		<script>
			
			$('#adding_date').datepicker({
				format : 'yyyy-mm-dd'
			});
			
		</script>	
		
<?php require_once "includes/footer.php";?>