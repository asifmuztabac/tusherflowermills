-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2016 at 10:41 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project2`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_list`
--

CREATE TABLE IF NOT EXISTS `account_list` (
`id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `account_list`
--

INSERT INTO `account_list` (`id`, `account_id`, `name`, `details`) VALUES
(2, 2, 'adafas', 'edtgddh'),
(3, 2, 'liton', 'sgsg'),
(4, 5, 'A', 'sdgs'),
(5, 2, 'B', 'sdgfsdg'),
(6, 5, 'B', 'sdgfsdg'),
(7, 5, 'C', 'sdgsdg');

-- --------------------------------------------------------

--
-- Table structure for table `account_name`
--

CREATE TABLE IF NOT EXISTS `account_name` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `account_name`
--

INSERT INTO `account_name` (`id`, `name`, `details`) VALUES
(2, 'Gomer Parti', 'This is Details Text\r\ners'),
(4, 'Another Account Name', 'This is Details'),
(5, 'Truck Rental', 'সসদ্গ r ');

-- --------------------------------------------------------

--
-- Table structure for table `all_names`
--

CREATE TABLE IF NOT EXISTS `all_names` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `all_names`
--

INSERT INTO `all_names` (`id`, `name`, `name_id`) VALUES
(16, 'Choy tara moyda 74 kg', 1),
(17, '74', 2),
(18, 'Choy Tara Moyda 50kg', 1),
(19, '50', 2),
(20, 'choy tara moyda 37kg', 1),
(21, '37', 2);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nid` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `joining_date` date NOT NULL,
  `designation` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `emplolee_id` varchar(255) NOT NULL,
  `sallery` int(11) NOT NULL,
  `present_address` varchar(255) NOT NULL,
  `permanent_address` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `nid`, `father_name`, `mother_name`, `mobile_number`, `joining_date`, `designation`, `image`, `emplolee_id`, `sallery`, `present_address`, `permanent_address`) VALUES
(1, 'Mehedi hasan', '21423423423532453435435', '??????????', '', '3453534534', '2016-05-09', 'sdfsdfdsg', 'cfce32f2f99a2b96fc56ad171b63b89b.jpg', '44444', 20000, 'Natore', 'Natore'),
(2, 'Sabbir Ahmed', '34353534543543534635435', '?', '', '43534543534', '2016-05-23', 'dcgfdgd', 'f54cd353d5a78a49aa7713f58e4031ac.jpg', 'sdf4', 60000, 'Natore', 'Natore'),
(3, 'Asif', '1234567891234567890', 'Abdur', 'Formina', '123456789', '2016-06-06', 'emploee', '', '123456789', 2000, 'pabna', 'pabna');

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
`id` int(11) NOT NULL,
  `vouchar_no` varchar(255) CHARACTER SET utf16 NOT NULL,
  `account_id` varchar(255) CHARACTER SET utf16 NOT NULL,
  `sub_account_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `paid` varchar(255) NOT NULL,
  `due` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `details` varchar(255) CHARACTER SET utf16 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `vouchar_no`, `account_id`, `sub_account_id`, `amount`, `paid`, `due`, `date`, `details`) VALUES
(12, 'a', '5', 4, '11111', '11111', '0', '2016-06-28', ''),
(13, 'gdfg', '2', 3, '5000', '5000', '0', '2016-06-28', ''),
(15, 'dgd', '2', 5, '6000', '6000', '0', '2016-06-28', ''),
(16, 'gdfg', '2', 2, '34534534', '3453454', '31081080', '2016-06-28', ''),
(17, '', '2', 2, '0', '31081080', '0', '2016-06-28', ''),
(18, 'sdfsd', '5', 4, '5000', '4000', '1000', '2016-06-29', 'sdg sdg sh'),
(19, 'gdfg', '5', 4, '5000', '5000', '0', '2016-06-29', ''),
(20, 'dgd', '5', 4, '5345345', '5345340', '5', '2016-06-29', ''),
(21, 'sdfsd', '5', 6, '435436', '430000', '5436', '2016-06-29', ''),
(22, 'sdfsd', '5', 4, '23423', '23423', '0', '2016-06-29', ''),
(23, 'dg', '5', 4, '345345', '345345', '0', '2016-06-29', ''),
(24, 'gdfg', '5', 4, '45436', '45000', '436', '2016-06-29', ''),
(25, '', '5', 4, '0', '1000', '0', '2016-06-29', ''),
(26, '', '5', 4, '0', '441', '0', '2016-06-30', '');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
`id` int(11) NOT NULL,
  `customer_name` text NOT NULL,
  `customer_phone` text NOT NULL,
  `customer_address` text NOT NULL,
  `gate_pass` text NOT NULL,
  `name` int(11) NOT NULL,
  `sack_quantity` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `total_paid` int(11) NOT NULL,
  `total_due` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `customer_name`, `customer_phone`, `customer_address`, `gate_pass`, `name`, `sack_quantity`, `weight`, `total_cost`, `total_paid`, `total_due`, `date`, `status`) VALUES
(2, 'Asif', '123', 'Pabna', '123', 18, 5, 19, 250, 200, 50, '2016-06-27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sallery`
--

CREATE TABLE IF NOT EXISTS `sallery` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `basic` varchar(255) NOT NULL,
  `previous_arrears` varchar(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `overtime` varchar(255) NOT NULL,
  `fines` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `paid` varchar(255) NOT NULL,
  `dues` varchar(255) NOT NULL,
  `advance` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `sallery`
--

INSERT INTO `sallery` (`id`, `user_id`, `date`, `basic`, `previous_arrears`, `allowance`, `overtime`, `fines`, `total`, `paid`, `dues`, `advance`) VALUES
(14, 1, '2016-05-27', '20000', '0', '0', '0', '0', '0', '5000', '0', '5000'),
(15, 2, '2016-05-25', '60000', '10000', '0', '0', '0', '10000', '20000', '0', '10000');

-- --------------------------------------------------------

--
-- Table structure for table `sallery_details`
--

CREATE TABLE IF NOT EXISTS `sallery_details` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `basic` varchar(255) NOT NULL,
  `previous_arrears` varchar(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `overtime` varchar(255) NOT NULL,
  `fines` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `paid` varchar(255) NOT NULL,
  `dues` varchar(255) NOT NULL,
  `advance` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `sallery_details`
--

INSERT INTO `sallery_details` (`id`, `user_id`, `date`, `basic`, `previous_arrears`, `allowance`, `overtime`, `fines`, `total`, `paid`, `dues`, `advance`) VALUES
(16, 1, '2016-05-24', '20000', '0', '0', '0', '0', '20000', '10000', '10000', ''),
(17, 2, '2016-05-24', '60000', '0', '0', '0', '0', '60000', '50000', '10000', ''),
(18, 1, '2016-05-25', '0', '10000', '0', '0', '0', '10000', '10000', '0', ''),
(19, 2, '2016-05-25', '0', '10000', '0', '0', '0', '10000', '20000', '0', ''),
(20, 1, '2016-05-26', '0', '0', '5000', '0', '0', '5000', '5000', '0', '0'),
(21, 1, '2016-05-27', '0', '0', '0', '0', '0', '0', '5000', '0', '5000');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `shift` tinyint(4) NOT NULL,
  `todays_production` int(11) NOT NULL,
  `prev_stock` int(11) NOT NULL,
  `total_production` int(11) NOT NULL,
  `sack_quantity` int(11) NOT NULL,
  `total_quantity` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `product_id`, `shift`, `todays_production`, `prev_stock`, `total_production`, `sack_quantity`, `total_quantity`, `date`, `status`) VALUES
(10, 16, 1, 17, 10, 740, 10, 740, '2016-06-23', 1),
(11, 16, 2, 17, 15, 370, 5, 1110, '2016-06-23', 1),
(12, 18, 2, 19, 5, 250, 5, 250, '2016-06-23', 1),
(13, 16, 1, 17, 25, 740, 10, 1110, '2016-06-23', 1),
(14, 18, 1, 19, 10, 250, 5, 500, '2016-06-22', 1),
(15, 20, 2, 21, 0, 215, 5, 215, '2016-06-24', 1),
(16, 20, 1, 21, 5, 215, 5, 430, '2016-06-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `per_field` varchar(255) DEFAULT NULL,
  `address` text,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `phone_number`, `permission`, `per_field`, `address`, `photo`) VALUES
(1, 'Mehedi Hasan', 'mehedi', 'mehedi@dsf.com', '1a1dc91c907325c69271ddf0c944bc72', '01780528223', 'Admin', NULL, '', '76e4f0b88e8d7bf7980a2754f0a8011b.jpg'),
(2, 'Mehedi hasan', 'aaa', 'mehedihasansabbirmi@gmail.com', '1a1dc91c907325c69271ddf0c944bc72', '23425255', 'Others', 'admin-panel.php,users.php,sallery-sheet.php,add-sallery.php,single-sallery-sheet.php,employee-add.php,employees.php,employee-view.php,employee-edit.php,account.php,add-account-name.php', 'Natore', '1031cf9eb2a7017cbd25f3a99a0d4e3c.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_list`
--
ALTER TABLE `account_list`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `account_name`
--
ALTER TABLE `account_name`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_names`
--
ALTER TABLE `all_names`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `sallery`
--
ALTER TABLE `sallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sallery_details`
--
ALTER TABLE `sallery_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_list`
--
ALTER TABLE `account_list`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `account_name`
--
ALTER TABLE `account_name`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `all_names`
--
ALTER TABLE `all_names`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sallery`
--
ALTER TABLE `sallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sallery_details`
--
ALTER TABLE `sallery_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
