	<?php
		$title = "Add Sallery";
		 require_once "includes/header.php";
		 $employee=new Employee;
		 $sallery = new Sallery;

		 ?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content container-fluid">
				<div class="dash-block box-style">
						<h4 class="block-title">Add Sallery</h4>
					<?php
						$validate = new Validate(new ErrorHandler);

						if(isset($_POST['submit'])){
							$validate->check($_POST, [
								'name' => [
									'required' => true
								],
								'date' => [
									'required' => true
								],
								'total' => [
									'number' => true
								],
								'paid' => [
									'number' => true
								]
							]);

							if($validate->passed()){
								$salleryHelper = new SalleryHelper;
								if ($sallery->salleryExists($_POST['name'])) {
									
									$add =  $sallery->update([
										'date' => $_POST['date'],
										'previous_arrears' => $_POST['previous_arrears'],
										'allowance' => $_POST['allowance'],
										'overtime' => $_POST['overtime'],
										'fines' => $_POST['fines'],
										'total' => $_POST['total'],
										'paid' => $_POST['paid'],
										'dues' => $_POST['dues'],
										'advance' => $_POST['advance']
									], ['user_id', '=',  $_POST['name']]);
									

								}else{
									$add = $sallery->create([
										'user_id' => $_POST['name'],
										'date' => $_POST['date'],
										'previous_arrears' => $_POST['previous_arrears'],
										'basic' => $_POST['basic'],
										'allowance' => $_POST['allowance'],
										'overtime' => $_POST['overtime'],
										'fines' => $_POST['fines'],
										'total' => $_POST['total'],
										'paid' => $_POST['paid'],
										'dues' => $_POST['dues'],
										'advance' => $_POST['advance']
									]);
								}
								$add = $salleryHelper->create([
										'user_id' => $_POST['name'],
										'date' => $_POST['date'],
										'basic' => $_POST['basic'],
										'previous_arrears' => $_POST['previous_arrears'],
										'allowance' => $_POST['allowance'],
										'overtime' => $_POST['overtime'],
										'fines' => $_POST['fines'],
										'total' => $_POST['total'],
										'paid' => $_POST['paid'],
										'dues' => $_POST['dues'],
										'advance' => $_POST['advance']
										]);
								
								if ($add === true) {
									echo '<p class="alert alert-success fade in">New Sallery add succssfuly<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
								}else{
									echo '<p class="alert alert-danger fade in">There was problem adding sallery<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
								}
							}
						}
						
					?>
					<form action="<?php self_action();?>" method="post">
						<div class="row">
							<div class="col-md-9 col-md-offset-1">
								<div class="row">
									
									<div class="col-md-6">
										<div class="form-group<?php echo $validate->errors()->first('date') != null ? ' has-error' : '';?>">
											<label class="control-label" for="date">Date <span class="star">*</span></label>
											<input type="text" readonly="true" value="<?php echo date('Y-m-d');?>" required="true" placeholder="Enter Your ..." class="form-control" id="date" name="date">
											<?php echo $validate->errors()->first('date') != null ? '<p class="help-block">' . $validate->errors()->first('date') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group<?php echo $validate->errors()->first('name') != null ? ' has-error' : '';?>">
											<label class="control-label" for="name">Name <span class="star">*</span></label>
											<select required="true" name="name" id="name" class="form-control">
												<option value="">Select an Employee</option>
												<?php if (!empty($employee->get())) : foreach ($employee->get() as $em) :?>
												<option value="<?=$em->id?>"><?=$em->name;?></option>	
												<?php endforeach; endif;?>
											</select>
											<?php echo $validate->errors()->first('name') != null ? '<p class="help-block">' . $validate->errors()->first('name') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="basic">Basic</label>
											<input type="number" readonly="true" value="0" placeholder="Enter Your ..." class="form-control" id="basic" name="basic">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="previous_arrears">Previous arrears </label>
											<input type="number" value="0" readonly="true" placeholder="Enter Your ..." class="form-control" id="previous_arrears" name="previous_arrears">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="allowance">Allowance </label>
											<input type="number" value="0" class="form-control" id="allowance" name="allowance">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="overtime">Wages of Overtime </label>
											<input type="number" value="0" class="form-control" id="overtime" name="overtime">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="fines">Fines </label>
											<input type="number" value="0" class="form-control" id="fines" name="fines">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group<?php echo $validate->errors()->first('total') != null ? ' has-error' : '';?>">
											<label class="control-label" for="total">Total <span class="star">*</span></label>
											<input type="number" required="true" value="0" class="form-control" id="total" name="total">
											<?php echo $validate->errors()->first('total') != null ? '<p class="help-block">' . $validate->errors()->first('total') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group<?php echo $validate->errors()->first('paid') != null ? ' has-error' : '';?>">
											<label class="control-label" for="paid">Paid <span class="star">*</span></label>
											<input type="number" required="true" value="0" class="form-control" id="paid" name="paid">
											<?php echo $validate->errors()->first('paid') != null ? '<p class="help-block">' . $validate->errors()->first('paid') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="dues">Dues </label>
											<input type="number" readonly="true" value="0" class="form-control" id="dues" name="dues">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="advance">Advance </label>
											<input type="number" readonly="true" value="0" class="form-control" id="advance" name="advance">
										</div>
									</div>
								</div>
								<input type="submit" class="btn custom-btn" name="submit" value="submit">
							</div>
						</div>
						
					</form>
				</div>
			</div>
			
		</div>
		<script src="js/bootstrap-datepicker.js"></script>
			<script>
				$('#date').datepicker({
					format : 'yyyy-mm-dd'
				});

				$('#total').on('focus', function(){
					var allowance = parseInt($('#allowance').val()),
						overtime = parseInt($('#overtime').val()),
						previous_arrears = parseInt($('#previous_arrears').val()),
						fines = parseInt($('#fines').val()),
						cal = parseInt($('#basic').val());

						$(this).val(((cal + allowance + overtime + previous_arrears) - fines));
				});

				$('#paid').on('focus', function(){
					$(this).val($('#total').val());
				});

				$('#paid').on('keyup', function(){
					var total = parseInt($('#total').val()),
						paid = parseInt($(this).val());
					if(total >= paid){
						$('#dues').val((total - paid));
					}else{
						$('#dues').val(0);
					}

					if (total <= paid) {
						$('#advance').val((paid - total));
					}else{
						$('#advance').val(0);
					}
					
					
				});

				$('#name').on('change', function(){
					
					$.ajax({
						url : 'ajax/get-sallery.php',
						type : 'post',
						dataType : 'json',
						data : {
							id : $(this).val(),
							date : $('#date').val()
						},
						success : function(response){
							$('#basic').val(response.sallery);
							if(response.dues.length == 0){
								response = {dues : 0};
								$('#previous_arrears').val(response.dues);
							}else{
								$('#previous_arrears').val(response.dues);
							}
							
						}
					});
				});

			</script>
<?php require_once "includes/footer.php";?>