<?php
$title = "বিক্রয়";
require_once "includes/header.php";

$stock = new Stock;
$category = new Category;
$sell= new Sale;
$validate = new Validate(new ErrorHandler);

?><div class="right-side">
<?php include_once "includes/sub-header.php";?>
<div class="dash-content">
	<div class="dash-block box-style clearfix">
		<h4 class="block-title">Add New Sale</h4>

		<div class="col-md-10 col-md-offset-1">
			<?php


			if(!empty($_POST)){

				$validate->check($_POST, [
					'date' => [
					'required' => true
					],
					'customer_name' => [
					'required' => true
					],
					'name' => [
					'required' => true
					],
					'sack_quantity' => [
					'required' => true
					],
					'weight' => [
					'required' => true
					],
					'total_cost' => [
					'required' => true
					],
					'gate_pass' => [
					'required' => true
					],
					'customer_phone' => [
					'required' => true
					]
					]);
				if($validate->passed()){
					$add = $sell->create([
						'date' =>$_POST['date'],
						'customer_name' =>$_POST['customer_name'],
						'name' =>$_POST['name'],
						'customer_phone' =>$_POST['customer_phone'],
						'gate_pass' =>$_POST['gate_pass'],
						'total_due' =>$_POST['total_due'],
						'total_paid' =>$_POST['total_paid'],
						'customer_address' =>$_POST['customer_address'],
						'total_cost' =>$_POST['total_cost'],
						'sack_quantity' =>$_POST['sack_quantity'],
						'weight' =>$_POST['weight'],
						'status' => 1,

						]);

					if ($add) {
						echo '<p class="alert alert-success fade in">New Sale add successfully <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
					}else{
						echo '<p class="alert alert-danger fade in">There was problem adding Sale Report <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
					}
				}

			}

			?>
			<form action="<?php self_action();?>" method="post">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group<?php echo $validate->errors()->first('date') != null ? ' has-error':'';?>">
							<label for="adding_date" class="control-label">Date</label>
							<input type="text" readonly="true" value="<?php echo date('Y-m-d');?>" class="form-control" id="adding_date" name="date">
							<?php
							if($validate->errors()->first('date') != null){
								echo '<p class="help-block">'.$validate->errors()->first('date').'</p>';
							}
							?>
						</div>
					</div>
				</div>
				<div class="sell_block_group">
					<h4 class="block-title">Customer Info</h4>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group<?php echo $validate->errors()->first('customer_name') != null ? ' has-error':'';?>">
								<label for="customer_name" class="control-label">Customer Name <span class="star">*</span></label>
								<input type="text" name="customer_name" class="form-control">
								<?php
								if($validate->errors()->first('customer_name') != null){
									echo '<p class="help-block">'.$validate->errors()->first('customer_name').'</p>';
								}
								?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group<?php echo $validate->errors()->first('customer_address') != null ? ' has-error':'';?>">
								<label for="customer_address" class="control-label">Customer Address</label>
								<textarea type="text" name="customer_address" class="form-control"></textarea> 
								<?php
								if($validate->errors()->first('customer_address') != null){
									echo '<p class="help-block">'.$validate->errors()->first('customer_address').'</p>';
								}
								?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group<?php echo $validate->errors()->first('gate_pass') != null ? ' has-error':'';?>">
								<label for="gate_pass" class="control-label">Gate Pass(if available)</label>
								<input type="text" name="gate_pass" class="form-control">
								<?php
								if($validate->errors()->first('gate_pass') != null){
									echo '<p class="help-block">'.$validate->errors()->first('gate_pass').'</p>';
								}
								?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group<?php echo $validate->errors()->first('customer_phone') != null ? ' has-error':'';?>">
								<label for="customer_phone" class="control-label">Customer Phone No.<span class="star">*</span></label>
								<input type="text" name="customer_phone" class="form-control">
								<?php
								if($validate->errors()->first('customer_phone') != null){
									echo '<p class="help-block">'.$validate->errors()->first('customer_phone').'</p>';
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="sell_block_group">
					<h4 class="block-title">Product Info</h4>
					<div class="row">

						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group<?php echo $validate->errors()->first('name') != null ? ' has-error':'';?>">
										<label for="name" class="control-label">Product Name <span class="star">*</span></label>
										<select required="" name="name" id="name" class="form-control">
											<option value="">Select name..</option>
											<?php foreach($category->getCategory($category->getCategoryId('product_name')) as $cat) :?>
												<option value="<?=$cat->id;?>"><?=$cat->name;?></option>>
											<?php endforeach;?>
										</select>
										<?php
										if($validate->errors()->first('name') != null){
											echo '<p class="help-block">'.$validate->errors()->first('name').'</p>';
										}
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group<?php echo $validate->errors()->first('weight') != null ? ' has-error':'';?>">
										<label for="weight" class="control-label">Weight <span class="star">*</span></label>
										<select name="weight" id="weight" class="form-control">
											<option value="">Select weight..</option>
											<?php foreach($category->getCategory($category->getCategoryId('sack_weight')) as $cat) :?>
												<option value="<?=$cat->id;?>"><?=$cat->name;?></option>>
											<?php endforeach;?>


										</select>
										<?php
										if($validate->errors()->first('weight') != null){
											echo '<p class="help-block">'.$validate->errors()->first('weight').'</p>';
										}
										?>
									</div>

								</div>


							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group<?php echo $validate->errors()->first('sack_quantity_available') != null ? ' has-error':'';?>">
								<label for="sack_quantity_available" class="control-label">Sack Avail in Stock <span class="star">*</span></label>
								<input required="" type="text" class="form-control" name="sack_quantity_available" id="sack_quantity_available">
								<?php
								if($validate->errors()->first('sack_quantity_available') != null){
									echo '<p class="help-block">'.$validate->errors()->first('sack_quantity_available').'</p>';
								}
								?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group<?php echo $validate->errors()->first('sack_quantity') != null ? ' has-error':'';?>">
								<label for="sack_quantity" class="control-label">Sack Sale Amount <span class="star">*</span></label>
								<input required="" type="text" class="form-control" name="sack_quantity" id="sack_quantity">
								<?php
								if($validate->errors()->first('sack_quantity') != null){
									echo '<p class="help-block">'.$validate->errors()->first('sack_quantity').'</p>';
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="sell_block_group">
					<h4 class="block-title">Payment Info</h4>					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="total" class="control-label">Total Cost <span class="star">*</span></label>
								<input type="text" class="form-control" name="total_cost" id="total">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="total_paid" class="control-label">Total Paid <span class="star">*</span></label>
								<input type="text" class="form-control" name="total_paid" id="total_paid">
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="total_due" class="control-label">Total Due <span class="star">*</span></label>
								<input type="text" class="form-control" name="total_due" id="total_due">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<input type="hidden" name="previous_stock" value="" id="previous_stock">
						<input type="submit" value="Sale" class="btn custom-btn">
						<input type="reset" value="Reset" class="btn custom-btn">
					</div>
				</div>
			</div>
		</form>
	</div>

</div>
</div>
</div>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/sweetalert.min.js"></script>

<script>

	$('#adding_date').datepicker({
		format : 'yyyy-mm-dd'
	});
	$('#weight,#name').on('change', function(){
		var weight=$('#weight').val(), name=$('#name').val();
		$.ajax({
			url : 'ajax/check_product_availibility.php',
			type : 'post',
			dataType : 'json',
			data : {
				weight : weight,
				name : name
			},
			success: function(data){
				console.log(data);
				if (data==0) {
					$('#sack_quantity_available').val(0).attr('readonly','readonly');
					$('#sack_quantity').val(0).attr('readonly','readonly');
				}
				else{
				$('#sack_quantity_available').val(data);
				$('#sack_quantity').removeAttr('readonly');
				$('#sack_quantity').keyup(function(){
					var available=data, saleAmount=$('#sack_quantity').val();
					$('#sack_quantity_available').val(available);
					if(saleAmount>0){
						var rest= parseInt(available)-parseInt(saleAmount);

						if(rest<0){
							sweetAlert('Opps..','Sorry Stock Finished!','error');
							$('#sack_quantity_available').val(available);
							$('#sack_quantity').val(0);
						}else{
							$('#sack_quantity_available').val(rest);
						}
					}
					else{
						$('#sack_quantity_available').val(available);	
					}
				});
			}
		}
		});		
	});
	$('#total_paid').keyup(function(){
					var total_cost = parseInt($('#total').val()),
						total_paid = parseInt($(this).val());
						if(total_cost>=total_paid){
						$('#total_due').val(parseInt(total_cost - total_paid));
					}
					else{
						sweetAlert('Opps..','You can not do extra payment','error');
						$('#total_paid').val(0);
						$('#total_due').val(total_cost);
					}
					});
						

</script>

<?php require_once "includes/footer.php";?>