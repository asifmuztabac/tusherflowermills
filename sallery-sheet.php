<?php
	$title = "Dashboard | Main Page";
	require_once "includes/header.php";

	$sallery = new Sallery;
	$employee = new Employee;

	?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<div class="block-title">
						<a href="add-sallery.php" class="btn custom-btn">Add Sallery</a>
					</div>
				<?php if(!empty($sallery->get())) : ?>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>	
								<th>Name</th>	
								<th>Basic</th>	
								<th>Previous arrears</th>	
								<th>Allowance</th>	
								<th>Overtime</th>	
								<th>Fines</th>	
								<th>Total</th>
								<th>Paid</th>
								<th>Dues</th>
								<th>Advance</th>
								<th>Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($sallery->get() as $sa) : $em = $employee->getEmployeeForSallery($sa->user_id);?>
							<tr>
								<td><?php echo (isset($em->emplolee_id)) ? $em->emplolee_id: 'Not Found!';?></td>
								<td><?php echo (isset($em->name)) ? '<a href="single-sallery-sheet.php?user='. $sa->user_id .'">'.$em->name.'</a>': 'Not Found!';?></td>
								<td><?=$sa->basic;?></td>
								<td><?=$sa->previous_arrears;?></td>
								<td><?=$sa->allowance;?></td>
								<td><?=$sa->overtime;?></td>
								<td><?=$sa->fines;?></td>
								<td><?=$sa->total;?></td>
								<td><?=$sa->paid;?></td>
								<td><?=$sa->dues;?></td>
								<td><?=$sa->advance;?></td>
								<td><?=$sa->date;?></td>
								<td><?php echo $sallery->getPaid($sa->dues, $sa->id);?><a href="ajax/delete-item.php" id="<?=$sa->id;?>" data-table="<?php echo encryptMS('sallery');?>" onclick="delete_data(this); return false;">Delete</a></td>
							</tr>
						<?php endforeach;?>
						</tbody>
					</table>
					</div>
					
				<?php else :?>
					<h2>No data Found!</h2>
				<?php endif;?>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		
<?php require_once "includes/footer.php";?>