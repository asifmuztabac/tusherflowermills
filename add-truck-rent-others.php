<?php
	$title = "স্টক রেজিস্টার";
	require_once "includes/header.php";
	$stock = new Stock;
	$category = new Category;

	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<div class="block-title">
						<a href="add-stock.php" class="btn custom-btn">Add stock</a>
					</div>
					<?php if(!empty($stock->allData())) :?>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" style="text-align:center">
							<thead>
								<tr>
									<th rowspan="2" style="text-align:center;width:10%">Serial</th>	
									<th rowspan="2" style="text-align:center;width:25%">Name</th>	
									<th rowspan="2" style="text-align:center;width:10%">Previous Stock</th>	
									<th colspan="2" style="text-align:center;width:20%">Today's Stack</th>
									<th rowspan="2" style="text-align:center;width:10%">Total Stock</th>
								</tr>
								<tr>
									<th style="text-align:center">Day</th>
									<th style="text-align:center">Night</th>
								</tr>
							</thead>
							<tbody>
								<td>1.</td>
								<td>Flour</td>
								<td>12</td>
								<td>12</td>
								<td>25</td>
								<td>37</td>
							</tbody>
						</table>
					</div>
					<?php else : ?>
					<h2>No data Found!</h2>
					<?php endif;?>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		
<?php require_once "includes/footer.php";?>