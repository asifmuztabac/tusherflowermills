<?php
class Database {
	private static $instance = null;
	private $pdo, $query, $error = false, $results, $count = 0;

	private function __construct() {
		try {
			$this->pdo = new PDO("mysql:host=" . Config::get('mysql/host') . ";dbname=" . Config::get('mysql/db') . ";charset=utf8", Config::get('mysql/username'), Config::get('mysql/password'));
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public static function connect() {
		if(!isset(self::$instance)) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function query($sql, $params = []) {
		$this->error = false;
		if($this->query = $this->pdo->prepare($sql)) {
			if(count($params)){
				$x = 1;
				foreach ($params as $param) {
					$this->query->bindValue($x, $param);
					$x++;
				}
			}

			if($this->query->execute()) {
				$this->results = $this->query->fetchAll(PDO::FETCH_OBJ);
				$this->count = $this->query->rowCount();
			}else{
				$this->error = true;
			}

			return $this;
		}
		
	}

	public function insert($table, $fields = []){
		if(!empty($fields)){
			$keys = array_keys($fields);
			$value = '';
			$x = 1;

			foreach($fields as $field) {
				$value .= '?';
				if($x < count($fields)){
					$value .= ', ';
				}

				$x++;
			}

			$sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES($value)";
			if(!$this->query($sql,  $fields)->error()){
				return true;
			}
			
		}

		return false;
	}

	public function update($table, $fields = [], $where = []){
		$set = '';

		foreach($fields as $field => $value) {
			$set .= "{$field} = ?, ";
		}
		$set = rtrim($set, ', ');
		$sql = "UPDATE {$table} SET {$set} WHERE $where[0] $where[1] $where[2]";

		if(!$this->query($sql, $fields)->error()){
			return true;
		}
		return false;
	}



	public function pdo(){
		return $this->pdo;
	}

	public function results(){
		return $this->results;
	}

	public function count(){
		return $this->count;
	}

	public function error(){
		return $this->error;
	}

	public function first(){
		return (!empty($this->results())) ? $this->results()[0] : [];
	}

	public function delete($table, $field, $value){
		if($this->query = $this->pdo->prepare("DELETE FROM {$table} WHERE {$field} = ?")){
			if($this->query->execute([$value])){
				return true;
			}
		}

		return false;
	}

}