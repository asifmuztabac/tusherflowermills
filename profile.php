	<?php 
		$title = 'ব্যবহারকারী প্রোফাইল';
		require_once "includes/header.php";?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content clearfix">
						<div class="dash-block">
							<ul class="nav nav-tabs" role="tablist">
							    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">ব্যবহারকারির তথ্য </a></li>
							    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">পাসওয়ার্ড পরিবর্তন
							     </a></li>
							</ul>

							  <!-- Tab panes -->
							  <div class="tab-content">
							    <div role="tabpanel" class="tab-pane fade active in" id="home">
							    	<?php
							    		 
							    		$validator = new Validate(new ErrorHandler);
							    		if(isset($_POST['submit1'])){
							    			

							    			$validator->check($_POST, [
							    					 'name' => [
							    					 	'required' => true
							    					 ],
							    					 'email' => [
							    					 	'email' => true
							    					 ],
							    					 'phone_number' => [
							    					 	'required' => true,
							    					 	'number' => true
							    					 ]
							    				]);
							    			$validator->check($_FILES, [
							    						'image' => [
							    							'file_size' => 1,
															'file_type' => 'png,jpg,jpeg,gif'
							    						]
							    						
							    					]);

							    			if($validator->passed()){
							    		
							    				if(!empty($_FILES['image']['name'])){
							    					$name = uniqid();
													if($user->uploadPhoto('image',$name)){
														if(!empty($user->getUser()->photo)){
															unlink("uploads/{$user->getUser()->photo}");
														}
														$update_user = $user->updateUser([
															'name' => Input::get('name'),
															'email' => Input::get('email'),
															'phone_number' => Input::get('phone_number'),
															'address' => Input::get('address'),
															'photo' => $user->getImageName('image',$name)
														],['id','=', Session::get(Config::get('session/session_name'))]);

														if($update_user){
															echo '<p class="alert alert-success alert-dismissible fade in">ব্যবহারকারীর তথ্য পরিবর্তন করা হয়েছে <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	  <span aria-hidden="true">&times;</span>
																	</button></p>';
														}else{
															echo '<p class="alert alert-error alert-dismissible fade in">ব্যবহারকারীর তথ্য পরিবর্তন করা হয়নি , আবার চেস্টা করুন <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	  <span aria-hidden="true">&times;</span>
																	</button></p>';
														}

													}
							    				}else{
							    					$update_user = $user->updateUser([
															'name' => Input::get('name'),
															'email' => Input::get('email'),
															'phone_number' => Input::get('phone_number'),
															'address' => Input::get('address')
														],['id','=', Session::get(Config::get('session/session_name'))]);

													if($update_user){
														echo '<p class="alert alert-success alert-dismissible fade in">ব্যবহারকারীর তথ্য পরিবর্তন করা হয়েছে <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																  <span aria-hidden="true">&times;</span>
																</button></p>';
													}else{
														echo '<p class="alert alert-error alert-dismissible fade in">ব্যবহারকারীর তথ্য পরিবর্তন করা হয়নি , আবার চেস্টা করুন <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																  <span aria-hidden="true">&times;</span>
																</button></p>';
													}
							    				}
							    				
							    			}
							    		}
							    		$profile = $user->getUser();

							    		$perpp = ['Admin' => 'এডমিন', 'Others' => 'অন্যান্য'];
							    	?>

							    	<form action="<?php self_action();?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
										<div class="row">
											<div class="col-md-8">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group<?php echo !empty($validator->errors()->first('name')) ? ' has-error' : '';?>">
															<label class="control-label" for="name">নাম <span class="star">*</span></label>
															<input type="text" name="name" value="<?php echo $profile->name;?>" id="name" class="form-control" placeholder="আপনার নাম লিখুন">
															<?php echo !empty($validator->errors()->first('name')) ? '<p class="help-block">' . $validator->errors()->first('name') . '</p>' : '';?>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label" for="username">ইউজার নাম <span class="star">(এই তথ্যটি পরিবর্তন  যোগ্য নয়)</span></label>
															<input type="text" readonly="true" value="<?php echo $profile->username;?>" id="username" class="form-control" placeholder="Enter username">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group<?php echo !empty($validator->errors()->first('email')) ? ' has-error' : '';?>">
															<label class="control-label" for="email">ইমেল</label>
															<input type="email" value="<?php echo $profile->email;?>" name="email" id="email" class="form-control" placeholder="আপনার ইমেল লিখুন">
															<?php echo !empty($validator->errors()->first('email')) ? '<p class="help-block">' . $validator->errors()->first('email') . '</p>' : '';?>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group<?php echo !empty($validator->errors()->first('phone_number')) ? ' has-error' : '';?>">
															<label class="control-label" for="phone_number">ফোন নম্বর<span class="star">*</span> </label>
															<input type="text" value="<?php echo $profile->phone_number;?>" name="phone_number" id="phone_number" class="form-control" placeholder="আপনার ফোন নম্বর লিখুন">
															<?php echo !empty($validator->errors()->first('phone_number')) ? '<p class="help-block">' . $validator->errors()->first('phone_number') . '</p>' : '';?>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label" for="permission">ব্যবহারকারির অনুমতি <span class="star">*</span> </label>
															<input type="text" readonly="true" class="form-control" id="permission" value="<?php echo $perpp[$profile->permission];?>">
														</div>
													</div>
													<div class="col-md-12">
													<div class="form-group<?php echo !empty($validator->errors()->first('image')) ? ' has-error' : '';?>">
														<label class="control-label" for="photo">ব্যবহারকারীর ছবি </label>
														<input type="file" name="image" id="photo" class="form-control">
														
														<p class="help-block">অনুগ্রহ করে ছবির সাইজ ২০০x২০০ অথবা এরচেয়ে বেশি চৌকনা ছবি ব্যবহার করুন</p>
														<?php echo !empty($validator->errors()->first('image')) ? '<p class="help-block">' . $validator->errors()->first('image') . '</p>' : '';?>
													</div>
												</div>
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label" for="address">ঠিকানা</label>
															<textarea name="address" class="form-control" id="address" rows="4" placeholder="আপনার ঠিকানা লিখুন"><?php echo $profile->address;?></textarea>
														</div>
													</div>
												</div>
												
											</div>
											<div class="col-md-4">
											<div class="box-style">
												<img src="<?php echo (!empty($profile->photo)) ? "uploads/" . $profile->photo : "images/user.png" ;?>" style="border-radius: 4px;border:1px solid #ddd;" class="img-responsive" alt="User Image">
											</div>
												
											</div>
											<div class="col-md-12">
												<input type="submit" name="submit1" class="btn custom-btn" value="Submit" />
											</div>
											
										</div>
									</form>
							    </div>
							    <div role="tabpanel" class="tab-pane fade" id="messages">
								
									<form action="ajax/change-pass.php" method="post" id="pass">
										<div class="row">
											<div class="col-md-5 col-md-offset-3 box-style loading-form">
												<div class="messages">
													
												</div>
												<div class="form-group">
													<label for="old_password" class="control-label">আগের পাসওয়ার্ড</label>
													<input type="password" class="form-control" placeholder="আগের পাসওয়ার্ড দিন" id="old_password" name="old_password">
												</div>
												<div class="form-group">
													<label for="new_password" class="control-label">নতুন পাসওয়ার্ড</label>
													<input type="password" class="form-control" placeholder="নুতুন পাসওয়ার্ড দিন" id="new_password" name="new_password">
												</div>
												<div class="form-group">
													<label for="new_password_again" class="control-label">পুনরায় নতুন পাসওয়ার্ড</label>
													<input type="password" class="form-control" placeholder="পুনরায় নতুন পাসওয়ার্ড দিন" id="new_password_again" name="new_password_again">
												</div>
												<input type="hidden" name="id" value="<?php echo Session::get(Config::get('session/session_name'));?>">
												<button class="btn custom-btn" id="password_change">Submit</button>
												<i class="fa fa-circle-o-notch fa-spin" id="spin" aria-hidden="true"></i>
											</div>
										</div>
										
									</form>
							    </div>
							  </div>
							
						</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		<script>
			$('#pass').on('submit', function(e){
				if(!$('#spin').hasClass('ms_spin')){
					$('#spin').addClass('ms_spin');
				}
				if($('.form-group').hasClass('has-error')){
					$('.help-block').remove();
					$('.form-group').removeClass('has-error');
				}
				
				
				var data = $(this).serializeArray(),
					action = $(this).attr('action'),
					type = $(this).attr('method');
				
				$.ajax({
					url : action,
					data : data,
					dataType : 'json',
					type : type,
					success : function(res) {
						if(res.valid == false){
							$.each(res, function(i,v){
								$('#pass').find('[name=' + i + ']').parent().addClass('has-error').append('<p class="help-block">'+ v +'</p>');
								
							});
						}else{
							$('.messages').html(res.message);
							$('#pass').find('input').val('');
						}
						
					},
					complete : function(){
						$('#spin').removeClass('ms_spin');
					}
				});
				
				 e.preventDefault();
			});
		</script>
<?php require_once "includes/footer.php";?>