<?php
	$title = "জমা খরচের হিসাব";
	require_once "includes/header.php";

	//$expenseHelper = new Expense;
	$accountHelper = new AccountName;
	$account = new Account;

	
	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
			

				<div class="dash-block box-style">
				<?php if (isset($_GET['expense'])) :
					$expense_id = $_GET['expense'];
					$accountInfo = $accountHelper->getAccountName($expense_id);
					$a = $account->getSubAccountName($expense_id)
					//$expenses = $expenseHelper->getExpense($expense_id);
				?> <?php if(!empty($a) AND !empty($accountInfo)) :?>
					<h4 class="block-title"><?=$accountInfo->name;?> এর সাথে টাকা আদান প্রদানের হিসাব</h4>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Serial</th>	
									<th>Account Name</th>	
									<th>Money transactions</th>	
									<th>Paid</th>
									<th>Due</th>
								</tr>
							</thead>
							<tbody>
								<?php $x=1; foreach($a as $data) :?>
								<tr>
									<td><?=$x;?></td>	
									<td><a href="expense-sub-full.php?account-id=<?=$data->account_id;?>&sub-account-id=<?=$data->sub_account_id;?>"><?=$data->name;?></a></td>	
									<td><?=$data->amount;?> Taka</td>	
									<td><?=$data->paid;?> Taka</td>
									<td><?php echo $data->due > 0 ? $data->due . 'Taka | <a href="expense-payment.php?account-id=' . $expense_id . '&sub-account-id=' . $data->sub_account_id . '">Payment</a>' : $data->due;?> </td>
								</tr>
								<?php $x++; endforeach;?>
							</tbody>
						</table>
					</div>
					<?php else : ?>
					<h2>No data Found!</h2>
					<?php endif; else : ?>
					<h2>Data not found !</h2>
					<?php endif;?>
				</div>
			

			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		
<?php require_once "includes/footer.php";?>