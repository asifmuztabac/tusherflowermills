	<?php
		$title = "Dashboard | Main Page";
		 require_once "includes/header.php";
		 $salleryHelper = new SalleryHelper();
		 ?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style col-md-12 block-space-bottom">
					<h3>স্বাগতম!</h3>
					<p>আপনি সফল ভাবে এই অ্যাপ্লিকেশনে লগিন করতে পেরেছেন । আমরা আশা করি আপনি এই অ্যাপ্লিকেশনটি সঠিক ভাবে ব্যবহার করতে পারবেন ।<br> নিচে কিছু নিয়মাবলি দেওয়া হলঃ</p>
					<ol>
						<li>এই অ্যাপ্লিকেশনে সকল প্রকার সংখ্যা জাতীয় তথ্য ইংরেজীতে দিতে হবে ।</li>
						<?php if($user->isAdmin()) : ?>
						<li>আপনি যেহেতু এডমিন তাই আপনি এই অ্যাপ্লিকেশনটির সকল পেজ ব্যবহার করতে পারবেন ।</li>
						<li>আপনি অন্যান্য ব্যবহারকারীর তথ্য কিছুটা পরিবর্তন করতে পারবেন ।</li>
						<li>অন্যান্য ব্যবহারকারী কোন কোন পেজ ব্যবহার করতে পারবে তা আপনি নির্ধারণ করে দিতে পারবেন ।</li>
						<?php else :?>
						<li>আপনি যেহেতু এডমিন নয় তাই আপনি এই অ্যাপ্লিকেশনটির সকল পেজ ব্যবহার করতে পারবেন না, যদি না আপনাকে অনুমতি দেওয়া হয় ।</li>
						<?php endif;?>
					</ol>
				</div>
				<div class="dash-block box-style col-md-6 block-space-bottom">
					<h4 class="block-title">বর্তমান অবস্থা</h4>
					
				</div>
			</div>
		</div>
<?php require_once "includes/footer.php";?>