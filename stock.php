<?php
$title = "স্টক রেজিস্টার";
require_once "includes/header.php";
$stock = new Stock;
$category = new Category;
$session= new Session;
?><div class="right-side">
<?php include_once "includes/sub-header.php";?>
<div class="dash-content">
	<div class="dash-block box-style">
		<div class="block-title">
			<a href="add-stock.php" class="btn custom-btn">Add stock</a>
			<a href="" class="btn custom-btn" id="showmonthstock">Show Monthly</a>
			<a href="" class="btn custom-btn" id="showyearstock">Show Yearly</a><br>
			<div class="selectMonth">
				<div class="form-group" id="month_date_div" style="display: none;">
					<label for="month_date">Select Month:</label>
					<input type="text" class="form-control" name="month_date" id="month_date_selector">
				</div>
			</div>

			<div class="selectMonth">
				<div class="form-group" id="year_date_div" style="display: none;">
					<label for="year_date">Select Year:</label>
					<input type="text" class="form-control" name="year_date" id="year_date_selector">
				</div>
			</div>  
		</div>

		<div class="block-title-sub">

			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<p>The List is Showing Todays Production</p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<p align="right">Select Another Date: </p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<input type="text" value="<?php echo date('Y-m-d');?>" class="form-control" id="adding_date" name="date">
				</div>

			</div>

		</div>

		<div class="table-responsive" id="dataTable">
			<?php


			if(empty($session->exists('date'))){
				$dataDate=date('Y-m-d');
				$dataDate=$stock->allDataDate($dataDate);
			}
			else{
				$date=$session->get('date');
				if(strlen($date)>=10){
					$dataDate=$stock->allDataDate($date);
							//var_dump($dataDate);
				}
				else if(strlen($date)==7){
					$dateexplode=explode('-', $date);
					$month=$dateexplode['1'];
					$year=$dateexplode['0'];
					$dataMonth=$stock->allMonthData($month,$year);
							//var_dump($dataMonth);
				}
				else if(strlen($date)==4){
							//var_dump($date);
					$dataYear=$stock->allYearData($date);
							//var_dump($dataYear);
				}
				else{

				}
			}
			$totalSackAmount='';
			$total_productionAmount='';
			?>
			<?php if(isset($dataDate)){if(!empty($dataDate)) : ?>
				<table class="table table-striped table-bordered table-hover tableDate" style="text-align:center" >
					<thead>
						<tr>
							<th rowspan="2" style="text-align:center;width:10%">ক্রমিক নং</th>	
							<th rowspan="2" style="text-align:center;width:10%">তারিখ</th>	

							<th rowspan="2" style="text-align:center;width:25%">বিবরণ</th>	
							<th rowspan="2" style="text-align:center;width:10%">পূর্বের জের</th>	
							<th colspan="2" style="text-align:center;width:20%">জমা আগত</th>
							<th rowspan="2" style="text-align:center;width:10%">মোট</th>
						</tr>
						<tr>
							<th style="text-align:center">রাত</th>
							<th style="text-align:center">দিন</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($dataDate as $datam) {?>

							<tr>
								<td><?=$datam->id;?></td>
								<td><?=$datam->date;?></td>
								
								<td><?php $p_id=$category->getProductName($datam->product_id); echo $p_id->name ;?></td>
								<td><?=$datam->prev_stock; ?></td>

								<td><?php 

									if($datam->shift==='2'){
										$night=$datam->sack_quantity;
									}
									if(empty($night)){
										$night=0;
										echo $night;
										//$night=0;
									} 
									else{
										echo $night; 
											//$night=0;
									} 
									?>

								</td>

								<td><?php 
									if($datam->shift==='1'){
										$day=$datam->sack_quantity;

									}
									if(empty($day)){
										$day=0;
										echo $day;
											//$day=0;
									} 
									else{
										echo $day; 
												//$day=0;
									}
									?>
								</td>
								<td><?php 
									if(empty($day))
									{
										$day=0;
									} 
									$t=$day+$night; 
									echo $t;
									$day=0;$night=0; 
									$totalSackAmount=$totalSackAmount+$t;
									?>
								</td>
							</tr>

							<?php }?>
							<tr>
								<td colspan="6" style="text-align:center;width:10%">মোট</td>
								<td><?=$totalSackAmount;?></td>
							</tr>
						</tbody>
					</table>

				</div>
			<?php else : ?>
			<h2 >No data Found!</h2>
		<?php endif;
	}
	else if(isset($dataMonth)){
		if (!empty($dataMonth)):


			?>
		<table class="table table-striped table-bordered table-hover" style="text-align:center" id="tableMonth">
			<thead>
				<tr>
					<th rowspan="2" style="text-align:center;width:10%">ক্রমিক নং</th>	
					<th rowspan="2" style="text-align:center;width:10%">তারিখ</th>	

					<th rowspan="2" style="text-align:center;width:25%">বিবরণ</th>	
					<th rowspan="০" style="text-align:center;width:10%">মোট উৎপাদন</th>	
					<th colspan="2" style="text-align:center;width:20%">মোট বস্তা</th>

				</tr>

			</thead>
			<tbody>
				<?php foreach ($dataMonth as $datam) {?>
					<tr>
						<td><?=$datam->id;?></td>
						<td><?=$datam->date;?></td>

						<td><?php $p_id=$category->getProductName($datam->product_id); echo $p_id->name ;?></td>
						<td><?=$datam->total_production; ?></td>
						<td><?=$datam->sack_quantity; ?></td>
					</tr>

					<?php 
					$totalSackAmount=$datam->sack_quantity+$totalSackAmount;
					$total_productionAmount=$datam->total_production+$total_productionAmount;
				}?>
				<tr>
					<td colspan="3" style="text-align:center;width:10%">মোট</td>
					<td><?=$total_productionAmount;?></td>
					<td><?=$totalSackAmount;?></td>
				</tr>
			</tbody>
		</table>
	</div>
<?php else : ?>
	<h2 >No data Found!</h2>
<?php endif;
}
else if(isset($dataYear)){
	if(!empty($dataYear)):					?>
		<table class="table table-striped table-bordered table-hover" style="text-align:center" id="tableMonth">
			<thead>
				<tr>
					<th rowspan="2" style="text-align:center;width:10%">ক্রমিক নং</th>	
					<th rowspan="2" style="text-align:center;width:10%">তারিখ</th>	

					<th rowspan="2" style="text-align:center;width:25%">বিবরণ</th>	
					<th rowspan="০" style="text-align:center;width:10%">মোট উঠপাদন</th>	
					<th colspan="2" style="text-align:center;width:20%">মোট বস্তা</th>

				</tr>

			</thead>
			<tbody>
				<?php foreach ($dataYear as $datam) {?>
					<tr>
						<td><?=$datam->id;?></td>
						<td><?=$datam->date;?></td>

						<td><?php $p_id=$category->getProductName($datam->product_id); echo $p_id->name ;?></td>
						<td><?=$datam->total_production; ?></td>
						<td><?=$datam->sack_quantity; ?></td>
					</tr>

					<?php 
					$totalSackAmount=$datam->sack_quantity+$totalSackAmount;
					$total_productionAmount=$datam->total_production+$total_productionAmount;
				}?>
				<tr>
					<td colspan="3" style="text-align:center;width:10%">মোট</td>
					<td><?=$total_productionAmount;?></td>
					<td><?=$totalSackAmount;?></td>
				</tr>								
			</tbody>
		</table>
	<?php else : ?>
	<h2 >No data Found!</h2>
<?php endif;
}
else{
	?>
	<h2 >Nothing Found!</h2>
	<?php }?>
</div>
</div>
<script src="js/sweetalert.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script>
	$('#adding_date').datepicker({
		format : 'yyyy-mm-dd'
	});
	$('#adding_date').on('changeDate',function(){
		var dval=$('#adding_date').val();
		$.ajax({
			url : 'ajax/get-stock.php',
			type : 'post',
			dataType : 'json',
			data : {
				date : dval
			},
			success: function(data){
				console.log(data);
				//$("#dataTable").load().fadeIn('fast');
				$("#dataTable").load(document.URL +  ' #dataTable');
			}
		});
	});
	$('#month_date_selector').datepicker({
		format : 'yyyy-mm',
		viewMode: "months", 
		minViewMode: "months"
	});
	$('#year_date_selector').datepicker({
		format : 'yyyy',
		viewMode: "years", 
		minViewMode: "years"
	});
	$('#showmonthstock').on('click',function(){
		$('#year_date_div').slideUp();
		$('#month_date_div').slideToggle();
		return false;
	});
	$('#showyearstock').on('click',function(){
		$('#month_date_div').slideUp();
		$('#year_date_div').slideToggle();
		return false;
	});
	$('#year_date_selector').on('changeYear',function(){
		var yval=$('#year_date_selector').val();
		$.ajax({
			url : 'ajax/get-stock.php',
			type : 'post',
			dataType : 'json',
			data : {
				date : yval
			},
			success: function(data){
				console.log(data);
					        			//$("#dataTable").load().fadeIn('fast');
					        			$("#dataTable").load(document.URL +  ' #dataTable');
					        		}
					        	});
	});
	$('#month_date_selector').on('changeMonth',function(){
		var mval=$('#month_date_selector').val();
		$.ajax({
			url : 'ajax/get-stock.php',
			type : 'post',

			data : {
				date : mval
			},
			success: function(data){
				console.log(data);
				$("#tableDate").remove();
				$("#dataTable").load(document.URL +  ' #dataTable');
			}
		});
	});
</script>
<?php $session->delete('date'); ?>		
<?php require_once "includes/footer.php";?>