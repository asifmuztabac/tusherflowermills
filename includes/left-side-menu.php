
<ul>
	<?php foreach($user->menus() as $menu) :?>
	<?php if($user->hasPermission($menu['link'])) :?>
	<li><a href="<?=$menu['link'];?>"<?php echo (basename($_SERVER['SCRIPT_NAME']) == $menu['link']) ? ' class="active"' : '';?>> <span class="fa fa-<?=$menu['icon']?>"></span> <?=$menu['text']?></a></li>
	<?php endif;?>
	<?php endforeach;?>
</ul>