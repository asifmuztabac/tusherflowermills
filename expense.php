<?php
	$title = "জমা খরচের হিসাব";
	require_once "includes/header.php";
	$expenseHelper = new Expense;

	$accountHelper = new AccountName;
	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<div class="block-title">
						<a href="add-expense.php" class="btn custom-btn">Add New</a>

												
					</div>
					
					<?php if(!empty($accountHelper->getExpense())) :?>

					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Serial</th>	

									<th>Name</th>	
									<th>Money transactions</th>	
									<th>Payments</th>	
									<th>Due</th>
								</tr>
							</thead>
							<tbody>
								<?php $x=1; foreach($accountHelper->getExpense() as $data) :?>
								<tr>
									<td><?=$x;?></td>
									<td><a href="expense-full.php?expense=<?=$data->id;?>"><?=$data->name;?></a></td>
									<td><?= $data->amount;?></td>
									<td><?= $data->paid;?></td>
									<td><?= $data->due;?></td>

								</tr>
								<?php $x++; endforeach;?>
							</tbody>
						</table>
					</div>
					<?php else : ?>
					<h2>No data Found!</h2>
					<?php endif;?>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		
<?php require_once "includes/footer.php";?>