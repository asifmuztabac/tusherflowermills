<?php
class User {
	protected $pdo, $session_name, $data;
	
	public function __construct(){
		$this->pdo = Database::connect();
		$this->session_name = Config::get('session/session_name');
	}

	protected function find($user){
		$field = (filter_var($user, FILTER_VALIDATE_EMAIL)) ? 'email' : 'username';

		$data = $this->pdo->query("SELECT * FROM users WHERE $field = ?", [$user]);

		if($data->count()){
			$this->data = $data->first();
			return true;
		}

		return false;
	}

	public function login($username, $password) {
		$user = $this->find($username);

		if($user){
			if($this->data()->password === md5($password)){
				Session::put($this->session_name, $this->data()->id);
				Redirect::to('index.php');
			}else{
				throw new Exception("Your password is incorrect");
				
			}
		}else{
			throw new Exception("This username is incorrect");
		}
	}

	public function updateUser($fields = [], $where = []){
		return $this->pdo->update('users', $fields, $where);
	}

	public function getUser(){
		$user = $this->pdo->query("SELECT * FROM users WHERE id = ?", [Session::get(Config::get('session/session_name'))]);

		return $user->first();
	}

	public function user($id){
		return $this->pdo->query("SELECT * FROM users WHERE id = ?", [$id])->first();
	}

	public function getAllUsers(){
		return $this->pdo->query("SELECT * FROM users")->results();
	}

	protected function data(){
		return $this->data;
	}

	public function isLogin(){
		return Session::exists(Config::get('session/session_name'));
	}

	public function getImageName($file,$name) {
		$file = $_FILES[$file]['name'];
		$end = explode('.', $file);
		$file_ext = end($end);

		return md5($name) . '.' . $file_ext;
	}
	public function uploadPhoto($field, $name){
		$file = $_FILES[$field]['name'];
		$file_tmp = $_FILES[$field]['tmp_name'];
		$end = explode('.', $file);
		$file_ext = end($end);
		$file_name = 'uploads/' . md5($name) . '.' . $file_ext;

		return (move_uploaded_file($file_tmp, $file_name)) ? true : false;

	}

	public function create($fields = []) {
		return ($this->pdo->insert('users', $fields)) ? true : false;
	}

	public function changePassword($pass){
		return $this->updateUser([
				'password' => md5($pass)
			],['id', '=', Session::get(Config::get('session/session_name'))]);
	}

	public function allPermissions(){
		return [
			'admin-panel.php' => 'এডমিন প্যানেল',
			'users.php' => 'ব্যবহারকারী দেখুন / পরিবর্তন করুন',
			'sallery-sheet.php' => 'বেতন রেজিস্টার বহি ',
			'add-sallery.php' => 'নতুন বেতন দেওয়া',
			'single-sallery-sheet.php' => 'বেতন রেজিস্টার বহি বিস্তারিত',
			'employee-add.php' => 'নতুন কর্মচারী তৈরি',
			'employees.php' => 'কর্মচারী দেখা ও মুছে ফেলা',
			'employee-view.php' => 'কর্মচারীর তথ্য বিস্তারিত দেখা',
			'employee-edit.php' => 'কর্মচারীর তথ্য পরিবর্তন করা',
			'account.php' => 'জমা খরচের হিসাব',
			'add-account-name.php' => 'Add Account Name'
			];
	}

	public function hasPermission($url) {
		$user = $this->user(Session::get(Config::get('session/session_name')));
		$fields = explode(',', $user->per_field);
		array_push($fields, 'index.php','profile.php');
		switch ($user->permission) {
			case 'Admin':
				return true;
				break;
			
			default:
				return in_array($url, $fields);
				
				break;
		}
		return false;
	}

	public function getPermission($id){
		return (isset($this->user($id)->permission)) ? $this->user($id)->permission : '';
	}

	public function isAdmin(){
		return $this->user(Session::get(Config::get('session/session_name')))->permission == 'Admin';
	}

	public function menus() {
		return [
			[
				'icon' => 'tachometer',
				'link' => 'index.php',
				'text' => 'নীড়পাতা'
			],
			[
				'icon' => 'user-plus',
				'link' => 'admin-panel.php',
				'text' => 'এডমিন প্যানেল'
			],
			[
				'icon' => 'users',
				'link' => 'employees.php',
				'text' => 'কর্মচারী বৃন্দ'
			],
			[
				'icon' => 'file-text-o',
				'link' => 'sallery-sheet.php',
				'text' => 'বেতন রেজিস্টার বহি'
			],
			[
				'icon' => 'book',
				'link' => 'account.php',
				'text' => 'জমা খরচের হিসাব'
			],
			[
				'icon' => 'pie-chart',
				'link' => 'stock.php',
				'text' => 'স্টক'
			],
			[
				'icon' => 'pie-chart',
				'link' => 'sell.php',
				'text' => 'বিক্রয়'
			],	
			[
				'icon' => 'money',
				'link' => 'expense.php',
				'text' => 'ব্যয়'
			]			
		];
	}
	
}