	<?php
		$title = "Add Employee";
		 require_once "includes/header.php";?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content container-fluid">
				<div class="dash-block box-style">
					<h4 class="block-title">Add Employee</h4>
					<?php
						$validate = new Validate(new ErrorHandler);

						if(isset($_POST['submit'])){
							$validate->check($_POST, [
								'name' => [
									'required' => true,
									'minlength' => 3,
									'maxlength' => 50
								],
								'nid' => [
									'required' => true,
									'number' => true,
									'minlength' => 19,
									'unique' => 'employees'
								],
								'mobile_number' => [
									'required' => true,
									'number' => true
								],
								'joining_date' => [
									'required' => true
								],
								'designation' => [
									'required' => true
								],
								'emplolee_id' => [
									'required' => true,
									'unique' => 'employees'
								],
								'sallery' => [
									'required' => true,
									'number' => true
								],
								'present_address' => [
									'required' => true

								],
								'permanent_address' => [
									'required' => true
								]
							]);

							$validate->check($_FILES, [
									'image' => [
										'file_type' => 'jpeg,jpg,png,gif',
										'file_size' => 5
									]
								]);

							if ($validate->passed()) {
								
								$employee = new Employee;

								$image_name = '';
								$image_upload = true;

								if(!empty($_FILES['image']['name'])){
									$img_name = uniqid();
									if($user->uploadPhoto('image', $img_name)){
										$image_name = $user->getImageName('image', $img_name);
									}else{
										$image_upload = false;
									}
								}

								if($employee->create([
										'name' => $_POST['name'],
										'nid' => $_POST['nid'],
										'father_name' => $_POST['father_name'],
										'mother_name' => $_POST['mother_name'],
										'mobile_number' => $_POST['mobile_number'],
										'joining_date' => $_POST['joining_date'],
										'designation' => $_POST['designation'],
										'image' => $image_name,
										'emplolee_id' => $_POST['emplolee_id'],
										'sallery' => $_POST['sallery'],
										'present_address' => $_POST['present_address'],
										'permanent_address' => $_POST['permanent_address']
									]) && $image_upload){
									echo '<p class="alert alert-success fade in">New employee add succssfuly<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
								}else{
									echo '<p class="alert alert-success fade in">There was problem creating new Employee<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
								}
								
							}
						}

					?>

					<form action="<?php self_action();?>" method="post" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('name') != null ? ' has-error' : '';?>">
								<label class="control-label" for="name">Name <span class="star">*</span></label>
									<input type="text" value="<?php echo (isset($_POST['name'])) ? $_POST['name'] : '';?>" name="name" id="name" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('name') != null ? '<p class="help-block">'. $validate->errors()->first('name') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('nid') != null ? ' has-error' : '';?>">
								<label class="control-label" for="nid">NID Number <span class="star">*</span></label>
									<input type="text" name="nid" id="nid" value="<?php echo (isset($_POST['nid'])) ? $_POST['nid'] : '';?>" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('nid') != null ? '<p class="help-block">'. $validate->errors()->first('nid') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label" for="name">Father Name </label>
									<input type="text" name="father_name" value="<?php echo (isset($_POST['father_name'])) ? $_POST['father_name'] : '';?>" id="name" class="form-control" placeholder="Enter Your Emploeey name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label" for="name">Mother Name</label>
									<input type="text" name="mother_name" value="<?php echo (isset($_POST['mother_name'])) ? $_POST['mother_name'] : '';?>" id="name" class="form-control" placeholder="Enter Your Emploeey name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('mobile_number') != null ? ' has-error' : '';?>">
								<label class="control-label" for="m_number">Mobile Number <span class="star">*</span></label>
									<input type="text" id="m_number" value="<?php echo (isset($_POST['mobile_number'])) ? $_POST['mobile_number'] : '';?>" name="mobile_number" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('mobile_number') != null ? '<p class="help-block">'. $validate->errors()->first('mobile_number') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group<?php echo $validate->errors()->first('joining_date') != null ? ' has-error' : '';?>">
								<label class="control-label" for="joining_date">Joining date <span class="star">*</span></label>
									<input type="text" id="joining_date" value="<?php echo (isset($_POST['joining_date'])) ? $_POST['joining_date'] : '';?>" name="joining_date" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('joining_date') != null ? '<p class="help-block">'. $validate->errors()->first('joining_date') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group<?php echo $validate->errors()->first('designation') != null ? ' has-error' : '';?>">
								<label class="control-label" for="designation">Designation <span class="star">*</span></label>
									<input type="text" id="designation" value="<?php echo (isset($_POST['designation'])) ? $_POST['designation'] : '';?>" name="designation" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('designation') != null ? '<p class="help-block">'. $validate->errors()->first('designation') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('image') != null ? ' has-error' : '';?>">
								<label class="control-label" for="image">Image </label>
									<input type="file" id="image" name="image" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('image') != null ? '<p class="help-block">'. $validate->errors()->first('image') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group<?php echo $validate->errors()->first('emplolee_id') != null ? ' has-error' : '';?>">
								<label class="control-label" for="emplolee_id">Employee ID <span class="star">*</span></label>
									<input type="text" id="emplolee_id" value="<?php echo (isset($_POST['emplolee_id'])) ? $_POST['emplolee_id'] : '';?>" name="emplolee_id" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('emplolee_id') != null ? '<p class="help-block">'. $validate->errors()->first('emplolee_id') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group<?php echo $validate->errors()->first('sallery') != null ? ' has-error' : '';?>">
								<label class="control-label" for="sallery">Sallery <span class="star">*</span></label>
									<input type="text" id="sallery" value="<?php echo (isset($_POST['sallery'])) ? $_POST['sallery'] : '';?>" name="sallery" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('sallery') != null ? '<p class="help-block">'. $validate->errors()->first('sallery') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('present_address') != null ? ' has-error' : '';?>">
								<label class="control-label" for="present_address">Present Address <span class="star">*</span></label>
									<textarea type="text" id="present_address" rows="4" name="present_address" class="form-control" placeholder="Enter Your Emploeey name"><?php echo (isset($_POST['present_address'])) ? $_POST['present_address'] : '';?></textarea>
									<?php echo $validate->errors()->first('present_address') != null ? '<p class="help-block">'. $validate->errors()->first('present_address') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('permanent_address') != null ? ' has-error' : '';?>">
								<label class="control-label" for="permanent_address">Permanent Address <span class="star">*</span></label>
									<textarea type="text" id="permanent_address" rows="4" name="permanent_address" class="form-control" placeholder="Enter Your Emploeey name"><?php echo (isset($_POST['permanent_address'])) ? $_POST['permanent_address'] : '';?></textarea>
									<?php echo $validate->errors()->first('permanent_address') != null ? '<p class="help-block">'. $validate->errors()->first('permanent_address') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-12">
								<input name="submit" class="btn custom-btn" type="submit" value="Submit">
							</div>
						</div>
					</form>
				</div>
			</div>
			<script src="js/bootstrap-datepicker.js"></script>
			<script>
				
				$('#joining_date').datepicker({
					format : 'yyyy-mm-dd'
				});
			</script>
		</div>
<?php require_once "includes/footer.php";?>