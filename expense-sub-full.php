<?php
$title = "জমা খরচের হিসাব";
require_once "includes/header.php";
if (!isset($_GET['account-id'], $_GET['sub-account-id'])) {
	return header("Location: 404.php");

}

$page = isset($_GET['page']) ? $_GET['page'] : 1;

$expense = new Expense;
$accountName = new AccountName;
$account = new Account;
$a1 = $accountName->getAccountName($_GET['account-id']);

$b1 = $account->getAName($_GET['sub-account-id'], $_GET['account-id']);
if (isset($_POST['start-date'], $_POST['end-date'])) {
	$c1 = $expense->getExpenseByDate($_GET['account-id'],$_GET['sub-account-id'],$_POST['start-date'], $_POST['end-date']);
	$serial = 1;
	$total_item = $expense->countItemWhere(['amount','paid', 'due'],['account_id','=',$_GET['account-id']],$_POST['start-date'], $_POST['end-date']," AND sub_account_id = {$_GET['sub-account-id']} ");
}else{

	$pagination = new Paginator($expense->getExpenseCount($_GET['account-id'],$_GET['sub-account-id'])->total, 3, $page, "expense-sub-full.php?account-id={$_GET['account-id']}&sub-account-id={$_GET['sub-account-id']}&page=(:num)");
	

	$c1 = $expense->getExpense($_GET['account-id'],$_GET['sub-account-id'], $pagination->getLimitFirst(), $pagination->getLimitLast());

	$serial = $pagination->getCurrentPageFirstItem();

	$total_item = $expense->countItemLimit(['amount','paid', 'due'],['account_id','=',$_GET['account-id'] ],$pagination->getLimitFirst(), $pagination->getLimitLast()," AND sub_account_id = {$_GET['sub-account-id']} ORDER BY id DESC");

}
?><div class="right-side">
<?php include_once "includes/sub-header.php";?>
<div class="dash-content">

	<div class="dash-block box-style">
		<h4 class="block-title"><?=$a1->name;?> এর <?=$b1->name;?> এর সাথে টাকা আদান প্রদানের হিসাব</h4>
		<div class="block-title">
			<div class="row">
				<div class="col-md-2">
					<a class="btn btn-primary" href="<?php echo self_action_q();?>">Refresh</a>
				</div>
				<form action="" method="post">
					<div class="col-md-3 col-sm-3">
						<input required type="text" readonly="true" value="<?php echo isset($_POST['start-date']) ? $_POST['start-date'] : '';?>"  placeholder="Start" name="start-date" id="start_date" class="form-control" />
					</div>
					<div class="col-md-3 col-sm-3">
						<input required type="text" readonly="true" value="<?php echo isset($_POST['start-date']) ? $_POST['end-date'] : '';?>"  name="end-date" placeholder="End" id="end_date" class="form-control" />
					</div>
					<div class="col-md-2">
						<button class="btn btn-success">Submit</button>
					</div>
				</form>
			</div>

		</div>
		<?php if(!empty($c1)) :?>
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Serial</th>	
							<th>Vouchar No</th>	
							<th>Amount</th>	
							<th>Paid</th>
							<th>Due</th>
							<th>Date</th>
							<th>Details</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php  foreach ($c1 as $data) :?>
							<tr>
								<td><?=$serial;?></td>	
								<td><?= $data->vouchar_no;?></td>	
								<td><?= $data->amount;?></td>	
								<td><?= $data->paid;?></td>
								<td><?= $data->due;?></td>
								<td><?= $data->date;?></td>
								<td><?= $data->details;?></td>
								<td><a href="ajax/delete-item.php" onclick="delete_data(this); return false;" id="<?=$data->id;?>" data-table="<?php echo encryptMS('expense');?>">Delete</a></td>
							</tr>
							<?php $serial++; endforeach;?>

							<tr class="total-item">
								<td colspan="2">Total</td>
								<td><?=$total_item->amount;?></td>
								<td><?=$total_item->paid;?></td>
								<td><?=$total_item->due;?></td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
							</tr>
						</tbody>
					</table>

					<?php echo !isset($_POST['start-date'], $_POST['end-date']) ? $pagination->toHTML() : '';?>
				</div>
			<?php else:?>
				<h2>Data Not Found !</h2>
			<?php endif;?>
		</div>


	</div>
</div>
<script src="js/sweetalert.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script>
	$('#start_date,#end_date').datepicker({
		format : 'yyyy-mm-dd'
	});
</script>
<?php require_once "includes/footer.php";?>