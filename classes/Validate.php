<?php
/**
* Validate Class 
*/			
class Validate {
	
	protected $errorhandler;

	protected $db;

	protected $user;

	protected $rules = ['required','maxlength','minlength','email','alnum','alpha','match','number', 'file_size', 'file_type', 'unique','password_exists'];
	protected $items;
 	protected $message = [
		'required' => 'The :field field is required.',
		'minlength' => 'The :field field must be a minimum of :rule_value character.',
		'maxlength' => 'The :field field must be a maximum of :rule_value character.',
		'email' => 'That is not a valid email address.',
		'alnum' => 'This :field must be alphanumeric.',
		'alpha' => 'This :field must be latter.',
		'number' => 'This :field must be number.',
		'match' => 'This :field field must match the :rule_value field.',
		'file_size' => 'This :field file must be a maximum of :rule_value Megabyte.',
		'file_type' => 'This :field file must be :rule_value format.',
		'unique' => 'This :field is already exists.',
		'password_exists' => 'This :field is incorrect'
	];

	function __construct(ErrorHandler $errorhandler){
		$this->errorhandler = $errorhandler;
		$this->db = Database::connect();
		$this->user = new User;

	}

	public function check($items, $rules = []) {

		$this->items = $items;

		foreach ($items as $item => $value) {
			if (in_array($item, array_keys($rules))) {
				$this->validate([
						'field' => $item,
						'value' => $value,
						'rules' => $rules[$item]
					]);
			}
		}

		return $this;
	}

	protected function validate($item) {
		$field = $item['field'];

		foreach ($item['rules'] as $rule => $rule_value) {
			if(in_array($rule, $this->rules)) {
				if (!call_user_func_array([$this, $rule], [$field, $item['value'], $rule_value])) {
					$this->errorhandler->addError(
						str_replace([':field', ':rule_value', '_'], [$field, $rule_value, ' '], $this->message[$rule]),
						$field
					);
				}
			}
		}
	}

	public function passed(){
		return !$this->errorhandler->hasErrors();
	}

	public function errors(){
		return $this->errorhandler;
	}

	protected function required($field, $value, $rule_value) {
		return !empty(trim($value));
	}

	protected function minlength($field, $value, $rule_value) {
		return mb_strlen($value) >= $rule_value;
	}

	protected function maxlength($field, $value, $rule_value) {
		return mb_strlen($value) <= $rule_value;
	}

	protected function alnum($field, $value, $rule_value) {
		return ctype_alnum($value);
	}

	protected function email($field, $value, $rule_value) {
		return (!empty($value)) ? filter_var($value, FILTER_VALIDATE_EMAIL) : true;
	}

	protected function alpha($field, $value, $rule_value) {
		return rtrim(ctype_alpha($value),' ');
	}

	protected function number($field, $value, $rule_value) {
		return is_numeric($value);
	}

	protected function match($field, $value, $rule_value) {
		return $value === $this->items[$rule_value];
	}

	protected function file_size($field, $value, $rule_value){
		return $this->items[$field]['size'] <= (($rule_value * 1024) * 1024);
	}

	protected function file_type($field, $value, $rule_value){
		$allowed_file_type = explode(',', $rule_value);
		$end = explode('.', $this->items[$field]['name']);
		$file_type = strtolower(end($end));
		return (empty($this->items[$field]['name']) || in_array($file_type, $allowed_file_type)) ? true : false;
	}

	protected function unique($field, $value, $rule_value){
		return !$this->db->query("SELECT {$field} FROM {$rule_value} WHERE $field = ?", [$value])->count() ? true : false;

		
	}

	protected function password_exists($field, $value, $rule_value) {
		return ($this->user->getUser()->password) == md5($value);
	}
	
}