<?php

/**
* Account Class
*/
class Account extends Section
{
	
	protected $table = 'account_list';

	public function getAccountName($id)
	{
		return $this->db->query("SELECT * FROM {$this->table} WHERE account_id = ?", [$id])->results();
	}

	public function getSubAccountName($id)
	{
		return $this->db->query("SELECT expense.account_id, expense.sub_account_id, account_list.name,SUM(expense.amount) AS amount,SUM(expense.paid) AS paid, (SUM(expense.amount) - SUM(expense.paid)) AS due, expense.date FROM account_list LEFT JOIN expense ON account_list.id = expense.sub_account_id  WHERE expense.account_id = ? GROUP BY account_list.id", [$id])->results();
	}

	public function getAName($id, $account_id)
	{
		return $this->db->query("SELECT name FROM {$this->table} WHERE id=? AND account_id= ?", [$id, $account_id])->first();
	}

	public function getPayment($id, $id2)
	{
		return $this->db->query("SELECT (SUM(expense.amount) - SUM(expense.paid)) AS due FROM account_list LEFT JOIN expense ON account_list.id = expense.sub_account_id  WHERE expense.account_id = ? AND expense.sub_account_id = ? GROUP BY account_list.id", [$id,$id2])->first();
	}
}