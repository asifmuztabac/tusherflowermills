<?php
require 'core/init.php';

Database::connect()->pdo()->query(
		"CREATE TABLE users (
			id INT (10) unsigned auto_increment primary key,
			name VARCHAR (255),
			username VARCHAR (255),
			email VARCHAR (255),
			password VARCHAR (255),
			phone_number VARCHAR(255),
			permission VARCHAR (255),
			per_field VARCHAR (255),
			address TEXT,
			photo VARCHAR (255)

		)"
	);