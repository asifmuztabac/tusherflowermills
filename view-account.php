<?php
	$title = "জমা খরচের হিসাব";
	if (!isset($_GET['account'])) {
		header("Location: account.php");
	}
	require_once "includes/header.php";
	$accountHelper = new Account;

	$allData = $accountHelper->getAccountName($_GET['account']);
	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<h4 class="block-title">
						All Account's Name
					</h4>
					<?php if(!empty($allData)) :?>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>No</th>	
									<th>Name</th>	
									<th>Details</th>	
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $x=1; foreach($allData as $data) :?>
								<tr>
									<td><?=$x;?></td>
									<td><?=$data->name;?></td>
									<td><?=$data->details;?></td>
									<td><a href="edit-account.php?account=<?=$data->id;?>">Edit</a> | <a href="ajax/delete-item.php" id="<?=$data->id;?>" onclick="delete_data(this); return false;" data-table="<?php echo encryptMS('account_list');?>">Delete</a></td>
								</tr>
								<?php $x++; endforeach;?>
							</tbody>
						</table>
					</div>
					<?php else : ?>
					<h2>No data Found!</h2>
					<?php endif;?>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		
<?php require_once "includes/footer.php";?>