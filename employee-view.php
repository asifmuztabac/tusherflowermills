	<?php
		$title = "View Employee Details";
		ob_start();
		require_once "includes/header.php";
		if(isset($_GET['employee'])){
			$employee_id = $_GET['employee'];
		}else{
		 	Redirect::to('employees.php');
		}
		$employee = new Employee;

		
		
		?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<h4 class="block-title">View Employee Details</h4>
					<?php if(!empty($employee->getEmployee($employee_id))) : $emplo= $employee->getEmployee($employee_id);?>
						<div class="row">
							<div class="col-md-8">
								<div class="table-responsive">
									<table class="table table-bordered">
								<tbody>
									<tr>
										<td>Name</td>
										<td><?=$emplo->name;?></td>
									</tr>
									<tr>
										<td>Father Name</td>
										<td><?=$emplo->father_name;?></td>
									</tr>
									<tr>
										<td>Mother Name</td>
										<td><?=$emplo->mother_name;?></td>
									</tr>
									<tr>
										<td>Mobile Number</td>
										<td><?=$emplo->mobile_number;?></td>
									</tr>
									<tr>
										<td>Joining Date</td>
										<td><?=$emplo->joining_date;?></td>
									</tr>
									<tr>
										<td>Designation</td>
										<td><?=$emplo->designation;?></td>
									</tr>
									<tr>
										<td>Present Address</td>
										<td><?=$emplo->present_address;?></td>
									</tr>
									<tr>
										<td>Designation</td>
										<td><?=$emplo->permanent_address;?></td>
									</tr>
									<tr>
										<td>User ID</td>
										<td><?=$emplo->emplolee_id;?></td>
									</tr>
									<tr>
										<td>National ID</td>
										<td><?=$emplo->nid;?></td>
									</tr>
									<tr>
										<td>Sallery</td>
										<td><?=$emplo->sallery;?> Taka</td>
									</tr>
									<tr>
										<td>Action</td>
										<td><a href="employee-edit.php?employee=<?=$emplo->id;?>">Edit</a>  | <a href="single-sallery-sheet.php?user=<?=$emplo->id;?>">View Sallery Details</a></td>
									</tr>
								</tbody>
									
								</table>
								</div>
								
							</div>
							<div class="col-md-4">
								<div class="box-style">
									<img src="<?php echo !empty($employee->getEmployee($employee_id)->image) ? "uploads/{$employee->getEmployee($employee_id)->image}" : 'images/user.png';?>" class="img-responsive" alt="Employee PHOTO">
								</div>
								
							</div>
						</div>
					<?php else : ?>

					<h2>There are no employee no Found!</h2>
					<?php endif;?>
				</div>
			</div>
		</div>
<?php require_once "includes/footer.php";?>