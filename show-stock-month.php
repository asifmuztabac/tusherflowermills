<?php
$title = "স্টক রেজিস্টার";
require_once "includes/header.php";
$stock = new Stock;
$category = new Category;
$session= new Session;
?><div class="right-side">
<?php include_once "includes/sub-header.php";?>
<div class="dash-content">
	<div class="dash-block box-style">
		<div class="block-title">
			<a href="add-stock.php" class="btn custom-btn">Add stock</a>
			<a href="show-stock-month.php" class="btn custom-btn">View Monthly Stock</a>
			<a href="add-stock.php" class="btn custom-btn">View Yearly Stock</a>
		</div>
		
		<div class="block-title-sub">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p>The List is Showing Todays Production</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<p align="right">Select Another Date: </p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<input type="text" value="" class="form-control" id="adding_date1" name="date">
					</div>

				</div>
			</div>
		</div>
		
		<div class="table-responsive" id="dataTable">
			<?php
			
			if(empty($session->exists('date'))){
				$date=date('Y-m');
			}
			else{
				$date=$session->get('date');
			}
			var_dump($date);
			?>
			<?php if(!empty($stock->allDataMonth($date))) : $data=$stock->allDataMonth($date); var_dump($date);?>
				<?php var_dump($data) ?>
				<table class="table table-striped table-bordered table-hover" style="text-align:center">
					<thead>
						<tr>
							<th rowspan="2" style="text-align:center;width:10%">ক্রমিক নং</th>	
							<th rowspan="2" style="text-align:center;width:10%">তারিখ</th>	
							
							<th rowspan="2" style="text-align:center;width:25%">বিবরণ</th>	
							<th rowspan="2" style="text-align:center;width:10%">পূর্বের জের</th>	
							<th colspan="2" style="text-align:center;width:20%">জমা আগত</th>
							<th rowspan="2" style="text-align:center;width:10%">মোট</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $datam) {?>
							<tr>
								<td><?=$datam->id;?></td>
								<td><?=$datam->date;?></td>
								
								<td><?php $p_id=$category->getProductName($datam->product_id); echo $p_id->name ;?></td>
								<td><?=$datam->prev_stock; ?></td>

								<td><?php 
									if($datam->shift==='2'){
										$night=$datam->sack_quantity;
									}
									if(empty($night)){
										$night=0;
										echo $night;
										//$night=0;
									} 
									else{
										echo $night; 
											//$night=0;
									} 
									?>

								</td>

								<td><?php 
									if($datam->shift==='1'){
										$day=$datam->sack_quantity;
									}
									if(empty($day)){
										$day=0;
										echo $day;
											//$day=0;
									} 
									else{
										echo $day; 
												//$day=0;
									}
									?>
								</td>
								<td><?php 
									if(empty($day))
									{
										$day=0;
									} 
									$t=$day+$night; 
									echo $t;
									$day=0;$night=0; 
									?>
								</td>
							</tr>
							
							<?php }?>
						</tbody>
					</table>
				</div>
			<?php else : ?>
				<h2>No data Found!</h2>
			<?php endif;?>
		</div>
	</div>
</div>
<script src="js/sweetalert.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script>
	$('#adding_date1').datepicker({
		format: "yyyy-mm",
		viewMode: "months", 
		minViewMode: "months"
	});
	$('#adding_date1').on('changeDate',function(){
					//var currMonth = new Date(this.date).getMonth() + 1;
					var dval=$('#adding_date1').val();
					console.log(dval);
					$.ajax({
						url : 'ajax/get-stock.php',
						type : 'post',
						dataType : 'json',
						data : {
							date : dval
						},
						success: function(data){
							console.log(data);
					        			//$("#dataTable").load().fadeIn('fast');
					        			$("#dataTable").load(document.URL +  ' #dataTable');
					        		}
					        	});
				});
			</script>
			<?php $session->delete('date'); ?>		
			<?php require_once "includes/footer.php";?>