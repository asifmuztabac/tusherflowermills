<?php
session_start();
//ini_set('display_errors', 'off');
date_default_timezone_set('Asia/Dhaka');
mb_internal_encoding("UTF-8");
include '../core/config.php';
spl_autoload_register(function($name){
	require_once "../classes/{$name}.php";
});

require_once '../functions/functions.php';