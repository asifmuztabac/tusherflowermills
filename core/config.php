<?php

$GLOBALS['config'] = [
		'mysql' => [
			'host' => '127.0.0.1',
			'username' => 'root',
			'password' => '',
			'db' => 'project2'
		],
		'session' => [
			'session_name' => 'user_idms'
		],
		'settings' => [
			'charset' => 'utf8'
		],
		'pagination' => [
			'pagination_count' => 10
		]

	];