<?php
	$title = "স্টক";
	require_once "includes/header.php";
	
	$stock = new Stock;
	$category = new Category;
	$validate = new Validate(new ErrorHandler);

	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style clearfix">
					<h4 class="block-title">Add New Stock</h4>
					
					<div class="col-md-10 col-md-offset-1">
						<?php

							
							if(!empty($_POST)){
								
								$validate->check($_POST, [
									'date' => [
										'required' => true
									],
									'shift' => [
										'required' => true
									],
									'name' => [
										'required' => true
									],
									'sack' => [
										'required' => true
									],
									'weight' => [
										'required' => true
									],
									'total' => [
										'required' => true
									]
								]);
								if($validate->passed()){

									$product_id = $_POST['name'];
									$sack_id = $_POST['weight'];

									if(isset($_POST['p_name'])){
										$category->create([
												'name' => $_POST['p_name'],
												'name_id' => $category->getCategoryId('product_name')
											]);

										$product_id = $category->getNameId($_POST['p_name'])->id;

									}
									if(isset($_POST['p_weight'])){
										$category->create([
												'name' => $_POST['p_weight'],
												'name_id' => $category->getCategoryId('sack_weight')
										]);
										$sack_id = $category->getNameId($_POST['p_weight'])->id;
									}
									$sack_quantity=$_POST['sack'];
									$totalStock=$stock->getPreviousStock($product_id,$sack_id );
									if (!empty($totalStock)) {
										foreach ($totalStock as $totalStocks) {
											$prev_stock=$totalStocks->prev_stock;
											$total_quantity=$totalStocks->total_production;
										}
									}
									else{
										$total_quantity=0;
										$prev_stock=0;
									}

									$prev_stock=$prev_stock+$_POST['sack'];
									
	
									$total_quantity=$total_quantity+$_POST['total'];

									$add = $stock->create([
											'product_id' => $product_id,
											'shift' => $_POST['shift'],
											'todays_production' => $sack_id,
											'total_production' =>$_POST['total'],
											'prev_stock'=>$prev_stock,
											'sack_quantity' => $_POST['sack'],
											'total_quantity' =>$total_quantity,
											'date' => $_POST['date'],
											'status' => 1,
											
										]);
									if ($add) {
										echo '<p class="alert alert-success fade in">New stock add successfully <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
									}else{
										echo '<p class="alert alert-danger fade in">There was problem adding stock <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
									}
								}
								
							}

						?>
						<form action="<?php self_action();?>" method="post">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group<?php echo $validate->errors()->first('date') != null ? ' has-error':'';?>">
										<label for="adding_date" class="control-label">Date</label>
										<input type="text" readonly="true" value="<?php echo date('Y-m-d');?>" class="form-control" id="adding_date" name="date">
										<?php
											if($validate->errors()->first('date') != null){
												echo '<p class="help-block">'.$validate->errors()->first('date').'</p>';
											}
										?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group<?php echo $validate->errors()->first('shift') != null ? ' has-error':'';?>">
										<label for="shift" class="control-label">Shift <span class="star">*</span></label>
										<select required="" name="shift" id="shift" class="form-control">
											<option value="">Select Shift..</option>
											<option value="1">Day</option>
											<option value="2">Night</option>
										</select>
										<?php
											if($validate->errors()->first('shift') != null){
												echo '<p class="help-block">'.$validate->errors()->first('shift').'</p>';
											}
										?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group<?php echo $validate->errors()->first('name') != null ? ' has-error':'';?>">
												<label for="name" class="control-label">Product Name <span class="star">*</span></label>
												<select required="" name="name" id="name" class="form-control">
													<option value="">Select name..</option>
													<?php foreach($category->getCategory($category->getCategoryId('product_name')) as $cat) :?>
														<option value="<?=$cat->id;?>"><?=$cat->name;?></option>>
													<?php endforeach;?>
													<option value="add">Add option</option>
												</select>
												<?php
													if($validate->errors()->first('name') != null){
														echo '<p class="help-block">'.$validate->errors()->first('name').'</p>';
													}
												?>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="display:none;" >
											<label for="p_name" class="control-label">Product Name <span class="star">*</span></label>
											<input type="text" required="" name="p_name" disabled="disabled" id="p_name" class="form-control">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group<?php echo $validate->errors()->first('sack') != null ? ' has-error':'';?>">
										<label for="sack" class="control-label">Sack <span class="star">*</span></label>
										<input required="" type="text" class="form-control" name="sack" id="sack">
										<?php
											if($validate->errors()->first('sack') != null){
												echo '<p class="help-block">'.$validate->errors()->first('sack').'</p>';
											}
										?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group<?php echo $validate->errors()->first('weight') != null ? ' has-error':'';?>">
										<label for="weight" class="control-label">Weight <span class="star">*</span></label>
										<select name="weight" id="weight" class="form-control">
											<option value="">Select weight..</option>
											<?php foreach($category->getCategory($category->getCategoryId('sack_weight')) as $cat) :?>
												<option value="<?=$cat->id;?>"><?=$cat->name;?></option>>
											<?php endforeach;?>
											<option value="add">Add option</option>
										</select>
										<?php
											if($validate->errors()->first('weight') != null){
												echo '<p class="help-block">'.$validate->errors()->first('weight').'</p>';
											}
										?>
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group" style="display:none">
										<label for="p_weight" class="control-label">Weight <span class="star">*</span></label>
										<input type="text" required="" disabled="true" class="form-control" name="p_weight" id="p_weight">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="total" class="control-label">Total <span class="star">*</span></label>
										<input type="text" class="form-control" name="total" id="total">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="hidden" name="previous_stock" value="" id="previous_stock">
									<input type="submit" value="submit" class="btn custom-btn">
								</div>
							</div>
						</form>
					</div>
					
				</div>
			</div>
		</div>
		<script src="js/bootstrap-datepicker.js"></script>
		<script>
			
			$('#adding_date').datepicker({
				format : 'yyyy-mm-dd'
			});

			$('#name').on('change', function(){
				if(this.value == 'add'){
					$('#p_name').removeAttr('disabled');
					$('#p_name').parent().slideDown();
				}else{
					$('#p_name').attr('disabled', 'true');
					$('#p_name').parent().slideUp();
				}
			});
			$('#weight').on('change', function(){
				if(this.value == 'add'){
					$('#p_weight').removeAttr('disabled');
					$('#p_weight').parent().slideDown();
				}else{
					$('#p_weight').attr('disabled', 'true');
					$('#p_weight').parent().slideUp();
				}
			});

	/*$('#weight').on('change', function(){

				var weight=$('#weight').val(), sack=$('#sack').val();
				var total=weight*sack;
				console.log(total);
				$('#total').val(total).prop("readonly",true);

			});*/

			
		</script>

<?php require_once "includes/footer.php";?>