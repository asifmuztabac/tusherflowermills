<?php
	$title = "জমা খরচের হিসাব";
	require_once "includes/header.php";
	$expenseHelper = new Expense;
	//$report= new Section;

	?><div class="right-side">
			<?php include_once "includes/sub-header.php";	
			
				if(isset($_POST['start-date']) && isset($_POST['start-date'])){
					$start_date = $_POST['start-date'];
					$end_date = $_POST['end-date'];
					$result=$expenseHelper->allReport($start_date,$end_date);
				}
			
			?>
			
			<div class="dash-content print_none">
				<div class="dash-block box-style">	
					<form action="" method="post">
						<div class="col-md-offset-3 col-md-3 col-sm-3">
							<input required type="text" readonly="true" value="<?php echo date('Y-m-d');?>"  placeholder="Start" name="start-date" id="start_date" class="form-control" />
						</div>
						<div class="col-md-3 col-sm-3">
							<input required type="text" readonly="true" value="<?php echo date('Y-m-d');?>"  name="end-date" placeholder="End" id="end_start" class="form-control" />
						</div>
						<div class="col-md-2">
							<button class="btn btn-success">Submit</button>
						</div>
					</form>
					<button id="print" class="btn btn-warning tra-print" onclick="window.print(); return false;">Print</button>
				</div>	
			</div>	

			<?php if(!empty($result)):?>
			
			<div class="dash-content">
				<div class="dash-block box-style">	
					<h2 style="text-align:center;margin-bottom:30px">মেসার্স তুষার ফ্লাওয়ার মিলস</h2>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Serial</th>	
									<th>Date</th>	
									<th>Vouchar no</th>	
									<th>Particulars</th>	
									<th>Amount</th>	
								</tr>
							</thead>
							<tbody>
								<?php $x=1;$totalAmount=0; foreach($result as $data) :?>
								<tr>
									<td><?=$x;?></td>
									<td><?=$data->date;?></td>
									<td><?=$data->vouchar_no;?></td>
									<td><?=$data->name;?></td>
									<td><?=$data->amount; $totalAmount=$totalAmount+$data->amount;?></td>
									
								</tr>
								<?php $x++; endforeach;?>
								<tr>
									<th colspan='4' style="text-align:right">Total</th>
									<td><?=$totalAmount;?></td>
								</tr>
							</tbody>
						</table>
					</div>
					<table class="table print-table">
						 <tr>
							<td style="text-align:center"><p>Accountant</p></td>
							<td style="text-align:center"><p>Verified By</p></td>
							<td style="text-align:center"><p>Approved By</p></td>
						 </tr>
					</table>					
				</div>
			</div>
			<?php else : ?>
			<div class="dash-content">
				<div class="dash-block box-style">				
					<h2 style="text-align:center">No data Found!</h2>	
				</div>
			</div>
			<?php endif;?>
		</div>
		<script src="js/bootstrap-datepicker.js"></script>
		<script>
			
			$('#start_date').datepicker({
				format : 'yyyy-mm-dd'
			});

			$('#end_start').datepicker({
				format : 'yyyy-mm-dd'
			});
			
		</script>	
		
		
		
<?php require_once "includes/footer.php";?>