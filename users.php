	<?php 
		$title = 'ব্যবহারকারীর বিবরণ';
		require_once "includes/header.php";
			if(empty($_GET['id'])){
				
				Redirect::to('index.php');
			}
			
				
				$allPermissions = [] ;
		?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content clearfix">
				<div class="dash-block">
					<ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">ব্যবহারকারির তথ্য</a></li>
					    <?php if(!($user->getPermission($_GET['id']) == 'Admin')) :?>
					    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">ব্যবহারকারির অনুমতি</a></li>
						<?php endif;?>
					</ul>

					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane fade active in" id="home">
					    	<?php
					    		 
					    		$validator = new Validate(new ErrorHandler);
					    		if(isset($_POST['submit1'])){
					    			

					    			$validator->check($_POST, [
					    					 'name' => [
					    					 	'required' => true
					    					 ],
					    					 'email' => [
					    					 	'email' => true
					    					 ],
					    					 'phone_number' => [
					    					 	'required' => true,
					    					 	'number' => true
					    					 ]
					    				]);

					    			if($validator->passed()){
				    					$update_user = $user->updateUser([
												'name' => Input::get('name'),
												'email' => Input::get('email'),
												'phone_number' => Input::get('phone_number'),
												'permission' => Input::get('permission'),
												'address' => Input::get('address')
											],['id','=', Input::get('id')]);

										if($update_user){
											echo '<p class="alert alert-success alert-dismissible fade in">ব্যবহারকারীর তথ্য পরিবর্তন করা হয়েছে <button type="button" class="close" data-dismiss="alert" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button></p>';
										}else{
											echo '<p class="alert alert-error alert-dismissible fade in">ব্যবহারকারীর তথ্য পরিবর্তন করা হয়নি , আবার চেস্টা করুন <button type="button" class="close" data-dismiss="alert" aria-label="Close">
													  <span aria-hidden="true">&times;</span>
													</button></p>';
										}
					    			}
					    		}
					    		
					    		$old_user = $user->user($_REQUEST['id']);


					    	?>

					    	<?php if(!empty($old_user)) :
					    		$perms =  $user->user($_GET['id'])->per_field;
					    		$allPermissions =  explode(',', $perms);

					    	?>
					    	<form action="<?php self_action_q();?>" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-8">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group<?php echo !empty($validator->errors()->first('name')) ? ' has-error' : '';?>">
													<label class="control-label" for="name">নাম <span class="star">*</span></label>
													<input type="text" name="name" value="<?php echo $old_user->name;?>" id="name" class="form-control" placeholder="আপনার নাম লিখুন">
													<?php echo !empty($validator->errors()->first('name')) ? '<p class="help-block">' . $validator->errors()->first('name') . '</p>' : '';?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="username">ইউজার নাম <span class="star">(এই তথ্যটি পরিবর্তন  যোগ্য নয়)</span></label>
													<input type="text" readonly="true" value="<?php echo $old_user->username;?>" id="username" class="form-control" placeholder="Enter username">

												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group<?php echo !empty($validator->errors()->first('email')) ? ' has-error' : '';?>">
													<label class="control-label" for="email">ইমেল </label>
													<input type="email" value="<?php echo $old_user->email;?>" name="email" id="email" class="form-control" placeholder="আপনার ইমেল লিখুন">
													<?php echo !empty($validator->errors()->first('email')) ? '<p class="help-block">' . $validator->errors()->first('email') . '</p>' : '';?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group<?php echo !empty($validator->errors()->first('phone_number')) ? ' has-error' : '';?>">
													<label class="control-label" for="phone_number">ফোন নম্বর <span class="star">*</span> </label>
													<input type="text" value="<?php echo $old_user->phone_number;?>" name="phone_number" id="phone_number" class="form-control" placeholder="আপনার ফোন নম্বর লিখুন">
													<?php echo !empty($validator->errors()->first('phone_number')) ? '<p class="help-block">' . $validator->errors()->first('phone_number') . '</p>' : '';?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="permission">ব্যবহারকারির অনুমতি <span class="star">*</span> </label>
													<select name="permission" id="permission" class="form-control">
														<option value="Admin"<?php echo ($old_user->permission == 'Admin') ? ' selected' : '';?>>এডমিন</option>
														<option value="Others"<?php echo ($old_user->permission == 'Others') ? ' selected' : '';?>>অন্যান্য</option>

													</select>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="address">ঠিকানা</label>
													<textarea name="address" class="form-control" id="address" rows="4" placeholder="আপনার ঠিকানা লিখুন"><?php echo $old_user->address;?></textarea>
												</div>
											</div>
										</div>
										
									</div>
									<div class="col-md-4">
									<div class="box-style">
										<img src="<?php echo (!empty($old_user->photo)) ? "uploads/" . $old_user->photo : "images/user.png" ;?>" style="border-radius: 4px;border:1px solid #ddd;" class="img-responsive" alt="User Image">
									</div>
										
									</div>
									<div class="col-md-12">
										<input type="submit" name="submit1" class="btn custom-btn" value="Submit" />
									</div>
									
								</div>
							</form>
						<?php else :?>
							<h2>This user Not Found!</h2>
						<?php endif;?>
					    </div>
					    <?php if(!($user->getPermission($_GET['id']) == 'Admin')) :?>
					    <div role="tabpanel" class="tab-pane fade" id="messages">
					    	<div id="message"></div>
							<form action="ajax/change-permission.php" method="post" id="form">
								<div class="row">
									<div class="col-md-8 col-md-offset-2 box-style">
										<?php foreach($user->allPermissions() as $per => $pervalue) :?>
										<div class="col-md-6">
											<div class="form-group">
												<label><input type="checkbox"<?php echo (in_array( $per, $allPermissions)) ? ' checked' : '';?> name="permisson[]" value="<?php echo $per;?>"> <p class="label-icon"><?php echo $pervalue;?></p></label>
											</div>
										</div>
										<?php endforeach;?>
										<input type="hidden" name="user_id" value="<?php echo $_GET['id'];?>">
										<input type="submit" class="btn custom-btn" value="Submit">
										<i class="fa fa-circle-o-notch fa-spin" id="spin" aria-hidden="true"></i>
									</div>
								</div>
							</form>
					    </div>
					<?php endif;?>
					  </div>
					
				</div>
			</div>
		</div>
	<script src="js/sweetalert.min.js"></script>

	<script type="text/javascript">
		$('#form').on('submit', function(e){
			$('#spin').addClass('ms_spin');
			var data = $(this).serializeArray(), action = $(this).attr('action');

			$.ajax({
				url : action,
				type : 'post',
				data : data,
				success : function(res){
					$('#message').html(res);
				},
				complete : function(){
					$('#spin').removeClass('ms_spin');
				}
			});

			e.preventDefault();
		});
	</script>
<?php require_once "includes/footer.php";?>