<?php

require_once '../core/inita.php';

if (isset($_POST['weight']) and isset($_POST['name'])) {
	$stock = new Stock;
	$category = new Category;
	$session= new Session;
	$sell= new Sale;
	$weight=$_POST['weight'];
	$name=$_POST['name'];
	$data=$stock->checkForStock($weight,$name);
	$dataSale=$sell->checkForSale($weight,$name);
	if (!empty($data)) {
		foreach ($data as $datam) {
			$prev_stock=$datam->prev_stock;
		}
	}
	else{
		$prev_stock=0;
	}
	if (!empty($dataSale)) {
		foreach ($dataSale as $datamSale) {
			$prev_sale=$datamSale->sack_quantity;
		}
	}
	else{
		$prev_sale=0;
	}
	if ($prev_sale>$prev_stock) {
		$grandPrevStock=0;
	}
	else{
	$grandPrevStock=$prev_stock-$prev_sale;
}
	echo json_encode($grandPrevStock);
}