<?php
	$title = "জমা খরচের হিসাব";
	require_once "includes/header.php";
	$accountHelper = new AccountName;

	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<div class="block-title">
						<a href="add-account-name.php" class="btn custom-btn">Add Section Name</a> <a href="add-sub-account-name.php" class="btn custom-btn">Add Account Name</a>
					</div>
					<?php if(!empty($accountHelper->get())) :?>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>No</th>	
									<th>Name</th>	
									<th>Details</th>	
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $x=1; foreach($accountHelper->get() as $data) :?>
								<tr>
									<td><?=$x;?></td>
									<td><a href="view-account.php?account=<?=$data->id;?>"><?=$data->name;?></a></td>
									<td><?=$data->details;?></td>
									<td><a href="edit-account-name.php?account=<?=$data->id;?>">Edit</a> | <a href="ajax/delete-item.php" id="<?=$data->id;?>" onclick="delete_data(this); return false;" data-table="<?php echo encryptMS('account_name');?>">Delete</a></td>
								</tr>
								<?php $x++; endforeach;?>
							</tbody>
						</table>
					</div>
					<?php else : ?>
					<h2>No data Found!</h2>
					<?php endif;?>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		
<?php require_once "includes/footer.php";?>