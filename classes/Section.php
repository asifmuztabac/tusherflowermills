<?php

/**
* Main Section Class
*/
class Section {

	protected $db;

	protected $table = '';
	
	public function __construct(){
		$this->db = Database::connect();
	}

	public function create($fields){
		return $this->db->insert($this->table, $fields);
	}

	public function update($fields, $where) {
		return $this->db->update($this->table, $fields, $where);
	}

	public function delete($field, $value){
		return $this->db->delete($this->table, $field, $value);
	}


	public function allDataDate($date) {
		
		return $this->db->query("SELECT * FROM {$this->table} where date='$date'")->results();
	}
	public function allData() {
		return $this->db->query("SELECT * FROM {$this->table}")->results();
	}

	public function loadAllData()
	{

		return $this->db->query("SELECT * FROM {$this->table}")->results();
	}
	
	public function allReport($sdate,$tdate){
		return $this->db->query("SELECT * FROM {$this->table} WHERE date BETWEEN '$sdate' AND '$tdate'")->results();
	}

	public function firstData($id){
		return $this->db->query("SELECT * FROM {$this->table} WHERE id = ?", [$id])->first();
	}
	
	public function countItem(array $fields){
		$field = '';
		foreach ($fields as $item) {
			$field .= "SUM({$item}) AS {$item}, "; 			
		}
		
		$field = rtrim($field, ', ');
		return $this->db->query("SELECT {$field} FROM {$this->table}")->results();
	}

	public function countItemLimit(array $fields, array $where, $start, $end, $sql = ''){
		$field = '';
		foreach ($fields as $item) {
			$field .= "SUM({$item}) AS {$item}, ";
		}
		$field2 = implode(', ', $fields);
		$field = rtrim($field, ', ');
		return $this->db->query("SELECT {$field} FROM (SELECT {$field2} FROM {$this->table} WHERE {$where[0]} {$where[1]} ? {$sql} LIMIT {$start},{$end}) as r ", [$where[2]])->first();
	}

	public function getPreviousStock($product_id,$sack_id){
		return $this->db->query("SELECT total_production,prev_stock FROM {$this->table} WHERE product_id = ? AND todays_production=?", [$product_id,$sack_id])->results();
	}
	public function allMonthData($month,$year){
		return $this->db->query("SELECT id,product_id,date,SUM(total_production) as total_production ,SUM(sack_quantity) as sack_quantity FROM {$this->table} WHERE MONTH(date)=$month and YEAR(date)=$year GROUP BY date,product_id")->results();
	}
	public function allYearData($year){
		return $this->db->query("SELECT id,product_id,MONTHNAME(date) as date,SUM(total_production) as total_production ,SUM(sack_quantity) as sack_quantity FROM {$this->table} t WHERE YEAR(t.date)=$year GROUP BY MONTH(t.date),product_id")->results();
	}
	public function allMonthDataSale($month,$year){
		return $this->db->query("SELECT id,name,date,SUM(total_paid) as total_paid ,SUM(sack_quantity) as sack_quantity FROM {$this->table} WHERE MONTH(date)=$month and YEAR(date)=$year GROUP BY date,name")->results();
	}
	public function allYearDataSale($year){
		return $this->db->query("SELECT id,name,MONTHNAME(date) as date,SUM(total_paid) as total_paid ,SUM(sack_quantity) as sack_quantity FROM {$this->table} t WHERE YEAR(t.date)=$year GROUP BY MONTH(t.date),name")->results();
	}
	public function checkForStock($sack_id,$product_id){
		return $this->db->query("SELECT prev_stock FROM {$this->table} WHERE product_id=$product_id and todays_production=$sack_id")->results();
	}
	public function checkForSale($sack_id,$product_id){
		return $this->db->query("SELECT sack_quantity FROM {$this->table} WHERE name=$product_id and weight=$sack_id")->results();
	}
	public function allDataDateDue($date) {
		
		return $this->db->query("SELECT * FROM {$this->table} where date='$date'")->results();
	}
	public function allMonthDataSaleDue($month,$year){
		return $this->db->query("SELECT id,customer_name,SUM(total_cost) as total_cost,SUM(total_paid) as total_paid ,SUM(total_due) as total_due FROM {$this->table} WHERE MONTH(date)=$month and YEAR(date)=$year GROUP BY customer_name")->results();		
	}
	public function allYearDataSaleDue($year){
		return $this->db->query("SELECT id,customer_name,SUM(total_cost) as total_cost,SUM(total_paid) as total_paid ,SUM(total_due) as total_due FROM {$this->table} WHERE YEAR(date)=$year GROUP BY customer_name")->results();		
	}

	public function countItemWhere(array $fields, array $where,$start,$end, $sql = '')
	{
		$field = '';
		foreach ($fields as $item) {
			$field .= "SUM({$item}) AS {$item}, ";
		}
		$field2 = implode(', ', $fields);
		$field = rtrim($field, ', ');
		return $this->db->query("SELECT {$field} FROM (SELECT {$field2} FROM {$this->table} WHERE {$where[0]} {$where[1]} ? AND (`date` BETWEEN ? AND ?) {$sql}) as r", [$where[2],$start,$end])->first();
	}
}


	