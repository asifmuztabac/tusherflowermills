<?php

/**
* Error Handler 
*/
class ErrorHandler
{
	
	protected $errors = [];

	public function addError($error, $key = null) {
		if($key) {
			$this->errors[$key][] = $error;
		}else {
			$this->errors[] = $error;
		}
	}

	public function allErrors($key = null) {
		return isset($this->errors[$key]) ? $this->errors[$key] : $this->errors;
	}

	public function hasErrors() {
		return count($this->allErrors()) ? true : false;
	}

	public function first($key) {
		return isset($this->allErrors()[$key][0]) ? $this->allErrors()[$key][0] : '';
	}
}