<?php
	$title = "জমা খরচের হিসাব";
	if (!isset($_GET['account-id'], $_GET['sub-account-id'])) {
		return header("Location: 404.php");
	}
	require_once "includes/header.php";

	$expense = new Expense;
	$accountName = new AccountName;
	$account = new Account;
	$validate = new Validate(new ErrorHandler);

	$a1 = $accountName->getAccountName($_GET['account-id']);
	$b1 = $account->getAName($_GET['sub-account-id'], $_GET['account-id']);
	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<h4 class="block-title"><?php echo $a1->name;?> এর <?php echo $b1->name;?> এর বাকি টাকা পরিশোধ করুন ।</h4>
					<?php
						if ($_POST) {
							$validate->check($_POST, [
								'due' => [
									'required' => true
								],
								'payment' => [
									'required' => true,
									'number' => true
								]
							]);

							if ($validate->passed()) {
								
								$add = $expense->create([
										'vouchar_no' => $_POST['vouchar'],
										'account_id' => $_GET['account-id'],
										'sub_account_id' => $_GET['sub-account-id'],
										'amount' => 0,
										'paid' => $_POST['payment'],
										'due' => 0,
										'date' => $_POST['date'],
										'details' => $_POST['details']
									]); 

								if ($add) {
									echo '<p class="alert alert-success">'.$_POST['payment'].' Taka Payment Successfully</p>';
								}

							}
						}


					?>
					<div class="row">
							
						<div class="col-md-6 col-md-offset-3 box-style">
							<form action="" method="post">
								<div class="form-group<?php echo $validate->errors()->first('due') != null ? ' has-error' : '';?>">
									<label for="due" class="control-label">Deu</label>
									<input type="number" readonly="" value="<?php echo $account->getPayment($_GET['account-id'], $_GET['sub-account-id'])->due;?>" class="form-control" id="due" name="due">

									<?php if($validate->errors()->first('due') != null) :
										 
										 echo '<p class="help-block">'.$validate->errors()->first('due').'</p>';
									 
									 endif;?>
								</div>
								<div class="form-group">
									<label for="vouchar">Vouchar </label>
									<input type="text" id="vouchar" name="vouchar" class="form-control" placeholder="Enter your Vouchar no">
								</div>
								<div class="form-group<?php echo $validate->errors()->first('payment') != null ? ' has-error' : '';?>">
									<label for="payment" class="control-label">Payment <span class="star">*</span></label>
									<input type="number" placeholder="Enter your payment amount" class="form-control" id="payment" name="payment">
									<?php if($validate->errors()->first('payment') != null) :
										 
										 echo '<p class="help-block">'.$validate->errors()->first('payment').'</p>';
									 
									 endif;?>
								</div>
								<div class="form-group">
									<label for="date">Date <span class="star">*</span></label>
									<input type="text" readonly="" value="<?php echo date('Y-m-d');?>" class="form-control" id="date" name="date">
								</div>
								<div class="form-group">
									<label for="details">Details</label>
									<textarea name="details" placeholder="Enter your payment details" id="details" rows="4" class="form-control"></textarea>
								</div>

								<button class="btn custom-btn">Payment</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
			
		<script src="js/bootstrap-datepicker.js"></script>
		<script>
			$('#date').datepicker({
				format : 'yyyy-mm-dd'
			});

		</script>
<?php require_once "includes/footer.php";?>