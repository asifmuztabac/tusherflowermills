	<?php
	$title = 'অ্যাডমিন প্যানেল';
	 require_once "includes/header.php";?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content padding-style">
				<div class="row">
					<div class="col-md-8">
						<div class="dash-block">
							<?php
					$errorHandler = new ErrorHandler;
					$validator = new Validate($errorHandler);

					if(isset($_POST['submit'])){
						$validator->check($_POST, [
								'name' => [
									'required' => true,
									'maxlength' => 60,
									'minlength'=> 3,
								],
								'username' => [
									'required' => true,
									'minlength' => 3,
									'maxlength' => 60,
									'alnum' => true,
									'unique' => 'users'
								],
								'email' => [
									'maxlength' => 255,
									'email' => true,
									'required' => true,
									'unique' => 'users'
								],
								'phone_number' =>[
									'required' => true,
									'number' => true,
									'unique' => 'users'
								],
								'permission' => [
									'required' => true
								],
								'password' => [
									'required' => true,
									'minlength' => 6
								],
								'repeat_password' => [
									'match' => 'password'
								]

							]);
						$validator->check($_FILES, [
								'image' => [
									'file_size' => 1,
									'file_type' => 'png,jpg,jpeg,gif'
								]
							]);

						if($validator->passed()){
							if(!empty($_FILES['image']['name'])){
								$name = uniqid();
								if($user->uploadPhoto('image',$name)){
									$create_user = $user->create([
										'name' => Input::get('name'),
										'username' => Input::get('username'),
										'email' => Input::get('email'),
										'password' => md5(Input::get('password')),
										'phone_number' => Input::get('phone_number'),
										'permission' => Input::get('permission'),
										'address' => Input::get('address'),
										'photo' => $user->getImageName('image',$name)
									]);

									if($create_user){
										echo '<p class="alert alert-success">New user create successfully</p>';
									}else{
										echo '<p class="alert alert-danger">There was a problem creating an user</p>';
									}

								}

							}else{
								if($user->create([
										'name' => Input::get('name'),
										'username' => Input::get('username'),
										'email' => Input::get('email'),
										'password' => md5(Input::get('password')),
										'phone_number' => Input::get('phone_number'),
										'permission' => Input::get('permission'),
										'address' => Input::get('address')
									])){

									echo '<p class="alert alert-success">New user create successfully</p>';

								}else{
									echo '<p class="alert alert-danger">There was a problem creating an user</p>';
								}
							}
						}
						

						
					}

				?>
							<h4 class="block-title">নতুন ব্যবহারকারী তৈরি</h4>
							<form action="<?php self_action();?>" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group<?php echo !empty($validator->errors()->first('name')) ? ' has-error' : '';?>">
											<label class="control-label" for="name"> নাম <span class="star">*</span></label>
											<input type="text" name="name" value="<?php echo (isset($_POST['name'])) ? $_POST['name'] : '';?>" id="name" class="form-control" placeholder="আপানার নাম লিখুন">
											<?php echo !empty($validator->errors()->first('name')) ? '<p class="help-block">' . $validator->errors()->first('name') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group<?php echo !empty($validator->errors()->first('username')) ? ' has-error' : '';?>">
											<label class="control-label" for="username">ইউজার নাম <span class="star">*</span></label>
											<input type="text" name="username" id="username" value="<?php echo (isset($_POST['username'])) ? $_POST['username'] : '';?>" class="form-control" placeholder="ইংরেজীতে আপানার ইউজার নাম লিখুন">
											<?php echo !empty($validator->errors()->first('username')) ? '<p class="help-block">' . $validator->errors()->first('username') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group<?php echo !empty($validator->errors()->first('email')) ? ' has-error' : '';?>">
											<label class="control-label" for="email"> ইমেল <span class="star">*</span></label>
											<input type="email" name="email" id="email" value="<?php echo (isset($_POST['email'])) ? $_POST['email'] : '';?>" class="form-control" placeholder="আপনার ইমেল লিখুন">
											<?php echo !empty($validator->errors()->first('email')) ? '<p class="help-block">' . $validator->errors()->first('email') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group<?php echo !empty($validator->errors()->first('phone_number')) ? ' has-error' : '';?>">
											<label class="control-label" for="number">ফোন নাম্বার<span class="star">*</span></label>
											<input type="text" name="phone_number" id="number" value="<?php echo (isset($_POST['phone_number'])) ? $_POST['phone_number'] : '';?>" class="form-control" placeholder="আপনার ফোন নাম্বার লিখুন">
											<?php echo !empty($validator->errors()->first('phone_number')) ? '<p class="help-block">' . $validator->errors()->first('phone_number') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group<?php echo !empty($validator->errors()->first('permission')) ? ' has-error' : '';?>">
											<label class="control-label" for="permission">অনুমতি <span class="star">*</span></label>
											<select name="permission" id="permission" class="form-control">
												<option value="">পদবি সিলেক্ট করুন</option>
												<option value="Admin">এডমিন</option>
												<option value="Others">অন্যান্য</option>
											</select>
											<?php echo !empty($validator->errors()->first('permission')) ? '<p class="help-block">' . $validator->errors()->first('permission') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group<?php echo !empty($validator->errors()->first('image')) ? ' has-error' : '';?>">
											<label class="control-label" for="photo">ব্যবহারকারীর ছবি </label>
											<input type="file" name="image" value="<?php echo (isset($_POST['image'])) ? $_POST['image'] : '';?>" id="photo" class="form-control">
											<?php echo !empty($validator->errors()->first('image')) ? '<p class="help-block">' . $validator->errors()->first('image') . '</p>' : '';?>
											<p class="help-block">অনুগ্রহ করে ছবির সাইজ ২০০x২০০ অথবা এরচেয়ে বেশি ব্যবহার করুন</p>
										</div>
									</div>
									
									
									<div class="col-md-6">
										<div class="form-group<?php echo !empty($validator->errors()->first('password')) ? ' has-error' : '';?>">
											<label class="control-label" for="password">পাসওয়ার্ড <span class="star">*</span></label>
											<input type="password" name="password" id="password" class="form-control" placeholder="পাসওয়ার্ড দিন">
											<?php echo !empty($validator->errors()->first('password')) ? '<p class="help-block">' . $validator->errors()->first('password') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group<?php echo !empty($validator->errors()->first('repeat_password')) ? ' has-error' : '';?>">
											<label class="control-label" for="repeat_password">পুনরায় পাসওয়ার্ড দিন <span class="star">*</span></label>
											<input type="password" name="repeat_password" id="repeat_password" class="form-control" placeholder="পুনরায় পাসওয়ার্ড দিন ">
											<?php echo !empty($validator->errors()->first('repeat_password')) ? '<p class="help-block">' . $validator->errors()->first('repeat_password') . '</p>' : '';?>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label" for="address">ঠিকানা</label>
											<textarea name="address" class="form-control" id="address" rows="4" placeholder="আপনার ঠিকানা লিখুন"><?php echo (isset($_POST['address'])) ? $_POST['address'] : '';?></textarea>
										</div>
									</div>
									<div class="col-md-12">
										<input type="submit" name="submit" class="btn custom-btn" value="Submit" />
									</div>
									
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-4">
						<div class="dash-block">
							<h4 class="block-title">List of user</h4>

							<ul class="list-user">
							<?php foreach($user->getAllUsers() as $single_user) :?>
								<li>
									<a href="<?php echo ($single_user->id == Session::get(Config::get('session/session_name'))) ? '#' : "users.php?id={$single_user->id}";?>"  class="clearfix">
										<div class="avater">
										<img src="<?php echo (!empty($single_user->photo)) ? "uploads/{$single_user->photo}" : "images/user.png";?>" class="img-responsive" alt="">
									</div>
									<div class="avater-name">
										<h3><?php echo $single_user->name;?></h3>
									</div>
									</a>
									<span class="user-action">
									<?php if(($single_user->id == Session::get(Config::get('session/session_name'))) == false) :?>
										 <a href="#" id = "<?php echo $single_user->id;?>" onclick="ms_alert(this); return false;">Delete</a>
									<?php 
										else: 
											echo "You are logged in";
									endif;?>


									</span>
								</li> 
							<?php endforeach;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		<script>
			function ms_alert(that) {
				swal({  
					title: "নিশ্চিত হউন",
					text: "আপনি কি নিশ্চিত এই ব্যবহারকারীকে মুছে ফেলবেন ?",
					type: "warning",   showCancelButton: true,
					closeOnConfirm: false,
					showLoaderOnConfirm: true,
					allowOutsideClick : true },
					function(){
						var id = that.id;
						$.ajax({
							url : 'ajax/delete-user.php',
							data : {id : id},
							type : 'post',
							dataType : 'json',
							success : function(res){
								if(res.delete == true){
									swal({title: "Success!",
										text: "এই ব্যবহারকারীকে এখন সফলভাবে মুছা হয়েছে",
										timer: 3000,
										showConfirmButton: true
									});

									$(that).parent('span').parent('li').remove();
								}else{
									swal({title: "Error!",
										text: "এই ব্যবহারকারীকে আপনি মুছতে পারবেন না",
										timer: 3000,
										showConfirmButton: true,
										'type' : 'error'
									});
								}
							}
						});

					
				});
			}
		</script>
<?php require_once "includes/footer.php";?>