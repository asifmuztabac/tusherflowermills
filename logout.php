<?php

require_once 'core/init.php';

Session::delete(Config::get('session/session_name'));

Redirect::to('login.php?info=logout');