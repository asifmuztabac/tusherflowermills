<?php

/**
* Sallery Class
*/
class Sallery extends Section
{
	protected $table = 'sallery';

	public function get(){
		return $this->db->query("SELECT * FROM {$this->table} ORDER BY `date` DESC")->results();
	}

	public function getDues($id){
		$count = $this->db->query("SELECT SUM(dues) AS dues FROM {$this->table} WHERE user_id = ?", [$id])->first();
		return $count->dues;
	}

	public function warning($id) {
		return ($this->getDues($id) > 0) ? '<span class="label label-info">Due</span>' : '';
	}

	public function getPaid($dues, $id){
		return ($dues > 0) ? '<a href="sallery-paid.php?paid=' . $id . '">Paid</a> | ' : '';
	}

	public function salleryExists($id){
		$user = $this->db->query("SELECT * FROM {$this->table} WHERE user_id = ?", [$id]);
		return ($user->count()) ? true : false;
	}

	public function detectDate($user_id){
		return $this->db->query("SELECT * FROM {$this->table} WHERE `user_id` = ? AND `date` BETWEEN ? AND ?", [$user_id, start_date_month(), end_date_month()])->count();
	}

	public function getAdvance($id){
		$count = $this->db->query("SELECT SUM(advance) AS advance FROM {$this->table} WHERE user_id = ?", [$id])->first();
		return $count->advance;
	}
}