<?php
	$title = "জমা খরচের হিসাব";
	require_once "includes/header.php";


	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">

					<h4 class="block-title">Add Account Section Name</h4>
					<?php 
						$validator = new Validate(new ErrorHandler);
						if (!empty($_POST)) {
							$validator->check($_POST, [
								'name' => [
									'required' => true,
									'maxlength' => 255
								],
								'details' => [
									'required' => true,

								]

							]);

							if($validator->passed()) {
								$account = new AccountName;

								$add = $account->create([
									'name' => $_POST['name'],
									'details' => $_POST['details']
								]);

								if ($add) {
									echo '<p class="alert alert-success fade in">New account name create successfully</p>';
								}


							}

						}
					?>
					<div class="row">
						<div class="col-md-6 col-md-offset-3 box-style">
							<form action="" method="post">
								<div class="form-group<?php echo $validator->errors()->first('name') != null ? ' has-error' : '';?>">
									<label for="name" class="control-label">Name <span class="star">*</span></label>
									<input type="text" name="name" class="form-control" id="name" placeholder="Enter Your name here">
									<?php echo $validator->errors()->first('name') != null ? '<p class="help-block">'. $validator->errors()->first('name') .'</p>' : '';?>
								</div>
								<div class="form-group<?php echo $validator->errors()->first('details') != null ? ' has-error' : '';?>">
									<label for="details" class="control-label">Details <span class="star">*</span></label>
									<textarea name="details" id="details" rows="5" placeholder="Enter data here" class="form-control"></textarea>
									<?php echo $validator->errors()->first('details') != null ? '<p class="help-block">'. $validator->errors()->first('details') .'</p>' : '';?>
								</div>
								<input type="submit" class="btn custom-btn" value="Submit">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
<?php require_once "includes/footer.php";?>