<?php
session_start();
//ini_set('display_errors', 'off');
mb_internal_encoding("UTF-8");
date_default_timezone_set('Asia/Dhaka');
require_once 'core/config.php';
spl_autoload_register(function($name){
	require_once "classes/{$name}.php";
});

require_once 'functions/functions.php';

