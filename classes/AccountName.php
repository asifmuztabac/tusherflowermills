<?php
/**
* This class is Sample of Full Account.
*/
class AccountName extends Section
{
	protected $table = 'account_name';
	

	public function get($item = '*'){
		return $this->db->pdo()->query("SELECT {$item} FROM {$this->table}")->fetchAll(PDO::FETCH_OBJ);
	}

	public function getExpense()
	{
		return $this->db->query("SELECT account_name.id,account_name.name, SUM(expense.amount)  as amount, SUM(expense.paid) AS paid, (SUM(expense.amount) - SUM(expense.paid)) AS due FROM expense LEFT JOIN account_name ON account_name.id = expense.account_id GROUP BY account_name.id")->results();
	}
	
	public function getAccountName($id)
	{
		return $this->db->query("SELECT id,name FROM {$this->table} WHERE id = ?", [$id])->first();
	}

}