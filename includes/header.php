<?php
	require_once 'core/init.php';
	$user = new User;

	if(!$user->isLogin()){
		Redirect::to('login.php?info=error');
	}elseif (!$user->hasPermission(basename($_SERVER['SCRIPT_NAME']))) {
		if(!(basename($_SERVER['SCRIPT_NAME']) == '404.php')){
			Redirect::to('404.php');
		}
		
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$title;?> | মেসার্স তুষার ফ্লাওয়ার মিলস</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/styleprint.css" media="print">
	<script src="js/jquery-1.12.3.min.js"></script>
</head>
<body>
	<div class="main">
		<div class="left-side" id="ms-left-side">
			<div class="left-header clearfix">
				<div class="avater">
					<img src="<?php echo (!empty($user->getUser()->photo)) ? "uploads/{$user->getUser()->photo}" : "images/user.png";?>" class="img-responsive" alt="">
				</div>
				<div class="avater-name">
					<h3><a href="profile.php" style="color: #fff;"><?php echo $user->getUser()->name;?></a></h3>
				</div>
			</div>
			<div class="left-menu">

				<?php include "includes/left-side-menu.php";?>
			</div>
		</div>