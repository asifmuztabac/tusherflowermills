	<?php
		$title = "Dashboard | Main Page";
		 require_once "includes/header.php";
		 $employee = new Employee;
		 $sallery = new Sallery;

		 ?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<div class="block-title">
						<a href="employee-add.php" class="btn custom-btn">Add new</a>
					</div>
					<?php if(!empty($employee->get())) :?>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>	
								<th>Name</th>	
								<th>NID</th>	
								<th>Mobile Number</th>	
								<th>Designation</th>	
								<th>Sallery</th>	
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($employee->get() as $employee) :?>
							<tr>
								<td><?=$employee->emplolee_id;?></td>
								<td><a href="employee-view.php?employee=<?=$employee->id;?>"><?=$employee->name;?></a> <?php echo $sallery->warning($employee->id);?></td>
								<td><?=$employee->nid;?></td>
								<td><?=$employee->mobile_number;?></td>
								<td><?=$employee->designation;?></td>
								<td><?=$employee->sallery;?> Taka</td>
								<td> <a href="employee-edit.php?employee=<?=$employee->id;?>">Edit</a> | <a href="javascript:void(0)" id="<?=$employee->id;?>" data-image="<?=$employee->image;?>" onclick="ms_alert(this);">Delete</a></td>
							</tr>
						<?php endforeach;?>
						</tbody>
					</table>
					</div>
					
				<?php else :?>
					<h3>Employee Empty</h3>
				<?php endif;?>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
		<script>
			function ms_alert(that) {
				swal({  
					title: "নিশ্চিত হউন",
					text: "আপনি কি নিশ্চিত এই কর্মচারীকে মুছে ফেলবেন ?",
					type: "warning",   showCancelButton: true,
					closeOnConfirm: false,
					showLoaderOnConfirm: true,
					allowOutsideClick : true},
					function(){
						var id = that.id,
							image_name = $(that).data('image');
						$.ajax({
							url : 'ajax/employee-delete.php',
							data : {id : id, image_name : image_name},
							type : 'post',
							dataType : 'json',
							success : function(res){
								if(res.delete == true){
									swal({title: "Success!",
										text: "এই কর্মচারীকে এখন সফলভাবে মুছা হয়েছে",
										timer: 3000,
										showConfirmButton: true
									});

									$(that).parents('tr').remove();
								}else{
									swal({title: "Error!",
										text: "এই কর্মচারীকে আপনি মুছতে পারবেন না",
										timer: 3000,
										showConfirmButton: true,
										'type' : 'error'
									});
								}
							}
						});

					
				});
			}
		</script>
<?php require_once "includes/footer.php";?>