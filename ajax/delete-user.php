<?php
require_once '../core/inita.php';
header("Content-type: text/javascript");
if(isset($_POST['id'])){
	$user_id = $_POST['id'];
	$db = Database::connect();
	$user = new User;
	$img = $db->query("SELECT * FROM users WHERE id = ?",[$user_id])->first();
	if($user->hasPermission('users.php')){
		if($db->delete('users', 'id', $user_id)){
		
		if(!empty($img->photo)){
			unlink("../uploads/" . $img->photo);
		}
		
			echo json_encode(['delete' => true]);

		}else{
			echo json_encode(['delete' => false]);
		}
	}else{
		echo json_encode(['delete' => false]);
	}
	

}