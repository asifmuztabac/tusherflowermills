<?php
	$title = "জমা খরচের হিসাব";
	require_once "includes/header.php";
	$account = new AccountName;
	$validate = new Validate(new ErrorHandler);

	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<h4 class="block-title">Add New Expense</h4>
					<?php 
						
						if (!empty($_POST)) {
							$validate->check($_POST, [
								'name' => [
									'required' => true
								],
								'account_name' => [
									'required' => true
								],
								'vou_no' => [
									'required' => true
								],
								'amount' => [
									'required' => true,
									'number' => true
								],
								'paid' => [
									'required' => true,
									'number' => true
								],
								'due' => [
									'number' => true
								],
								'date' => [
									'required' => true
								]

							]);
							if($validate->passed()) {
								$expense = new Expense;

								$add = $expense->create([
									'account_id' => $_POST['name'],
									'sub_account_id' => $_POST['account_name'],
									'vouchar_no' => $_POST['vou_no'],
									'amount' => $_POST['amount'],
									'paid' => $_POST['paid'],
									'due' => $_POST['due'],
									'date' => $_POST['date'],
									'details' => $_POST['details']
								]);

								if ($add) {
									echo '<p class="alert alert-success fade in">New Expenditure add successfully</p>';
								}


							}

						}
											
						
					?>
					<div class="row">
							<form action="" method="post">
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('name') != null ? ' has-error' : '';?>">
									<label for="name" class="control-label">Account Category<span class="star">*</span></label>
									<select required="" name="name" id="name" class="form-control">
										<option value="">Select item..</option>
										<?php if (!empty($account->get())) : foreach ($account->get() as $em) :?>
										<option value="<?=$em->id?>"><?=$em->name;?></option>	
										<?php endforeach; endif;?>
									</select>
									<?php
										if ($validate->errors()->first('name') != null) {
											echo '<p class="help-block">' . $validate->errors()->first('name') . '</p>';
										}

									?>
								</div>
								<div class="form-group<?php echo $validate->errors()->first('account_name') != null ? ' has-error' : '';?>">
									<label for="account_name" class="control-label">Account Category<span class="star">*</span></label>
									<select required="" name="account_name" id="account_name" class="form-control">
										
									</select>
									<?php
										if ($validate->errors()->first('account_name') != null) {
											echo '<p class="help-block">' . $validate->errors()->first('account_name') . '</p>';
										}

									?>
								</div>
								<div class="form-group<?php echo $validate->errors()->first('vou_no') != null ? ' has-error' : '';?>">
									<label for="vou_no" class="control-label">Vouchar No <span class="star">*</span></label>
									<input type="text" required="" name="vou_no" class="form-control" id="vou_no" placeholder="Enter Vouchar No here">
									<?php
										if ($validate->errors()->first('vou_no') != null) {
											echo '<p class="help-block">' . $validate->errors()->first('vou_no') . '</p>';
										}

									?>
								</div>								
								<div class="form-group<?php echo $validate->errors()->first('amount') != null ? ' has-error' : '';?>">
									<label for="amount" class="control-label">Amount <span class="star">*</span></label>
									<input type="text" placeholder="Enter your amount here" required="" name="amount" id="amount" class="form-control">
									<?php
										if ($validate->errors()->first('amount') != null) {
											echo '<p class="help-block">' . $validate->errors()->first('amount') . '</p>';
										}

									?>
								</div>
								
								<div class="form-group<?php echo $validate->errors()->first('paid') != null ? ' has-error' : '';?>">
									<label for="paid" class="control-label">Paid <span class="star">*</span></label>
									<input type="text" required="" placeholder="Enter paid amount here" name="paid" id="paid" class="form-control">
									<?php
										if ($validate->errors()->first('paid') != null) {
											echo '<p class="help-block">' . $validate->errors()->first('paid') . '</p>';
										}

									?>
								</div>
							</div>
								<div class="col-md-6">
								
									<div class="form-group<?php echo $validate->errors()->first('due') != null ? ' has-error' : '';?>">
									<label for="due" class="control-label">Due <span class="star">*</span></label>
									<input type="text" required="" placeholder="Enter due amount here" value="0" name="due" id="due" class="form-control">
									<?php
										if ($validate->errors()->first('due') != null) {
											echo '<p class="help-block">' . $validate->errors()->first('due') . '</p>';
										}

									?>
								</div>
								<div class="form-group<?php echo $validate->errors()->first('date') != null ? ' has-error' : '';?>">
									<label for="adding_date" class="control-label">Date <span class="star">*</span></label>
									<input type="text" readonly="true" value="<?php echo date('Y-m-d');?>" class="form-control" id="adding_date" name="date">
									<?php
										if ($validate->errors()->first('date') != null) {
											echo '<p class="help-block">' . $validate->errors()->first('date') . '</p>';
										}

									?>
								</div>
								<div class="form-group">
									<label for="details" class="control-label">Details</label>
									<textarea name="details" id="details" rows="5" placeholder="Enter data here" class="form-control"></textarea>
								</div>
								</div>
									<div class="col-md-12">
										<input type="submit" class="btn custom-btn" value="Submit">
									</div>							
								
							</form>
					</div>
				</div>
			</div>
		</div>
		<script src="js/bootstrap-datepicker.js"></script>
		<script>
			
			$('#adding_date').datepicker({
				format : 'yyyy-mm-dd'
			});

			$('#paid').on('focus', function() {
				$(this).val($('#amount').val());
			});
			
			$('#paid').on('blur', function() {
				var paid = $('#paid').val(),
					amount = $('#amount').val();
 
				$('#due').val((amount - paid));
			});

			$('#name').on('change', function(){
				$.ajax({
					url : 'ajax/get-account-name.php',
					type : 'POST',
					data : {
						id : $(this).val()
					},
					success : function(res) {
						$('#account_name').html(res);
					}
				});

			});
		</script>		
		
<?php require_once "includes/footer.php";?>