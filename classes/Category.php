<?php
/**
* Category Class

*/
class Category extends Section
{
	protected $table = 'all_names';

	protected $category = [
		'product_name' => 1,
		'sack_weight' => 2,

	];


	public function getCategoryId($name) {
		return $this->category[$name];
	}

	public function getCategory($id){
		return $this->db->query("SELECT * FROM {$this->table} WHERE name_id = ?", [$id])->results();
	}

	public function getNameId($where) {
		return $this->db->query("SELECT * FROM {$this->table} WHERE name = ?", [$where])->first();
	}
	public function getProductName($id){
		return $this->db->query("SELECT name FROM {$this->table} WHERE id = ?", [$id])->first();
	}
}