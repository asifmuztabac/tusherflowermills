<?php

require_once '../core/inita.php';

if (isset($_POST['id'])) {
	$sallery = new Sallery;
	$count = (new Employee)->getEmployee($_POST['id']);
	$basic = ($sallery->detectDate($_POST['id'])) ? 0 : $count->sallery;
	$dues = empty($sallery->getDues($_POST['id'])) ? ($sallery->getAdvance($_POST['id']) == 0) ? 0 : '-' . $sallery->getAdvance($_POST['id'])  : $sallery->getDues($_POST['id']);
	echo json_encode(['sallery' => $basic, 'dues' => $dues]);
}