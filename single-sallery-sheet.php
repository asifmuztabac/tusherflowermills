<?php

	$title = "Single Sallery Sheet";
	ob_start();
	require_once "includes/header.php";
	if(isset($_GET['user'])){
		$user_id = $_GET['user'];
	}else{
		Redirect::to('sallery-sheet.php');
	}
	$page = isset($_GET['page']) ? $_GET['page'] == 0 ? 1 : $_GET['page'] : 1;
	$sallery = new Sallery;
	$employee = new Employee;

	$salleryHelper = new SalleryHelper;
	$parpage = Config::get('pagination/pagination_count');
	$pagination =  new Paginator($salleryHelper->totalSallery($user_id),$parpage,$page,"?page=(:num)&user={$user_id}");
	$start_page = $pagination->getLimitFirst();

	$counting = $salleryHelper->countItemLimit(['basic', 'previous_arrears', 'allowance', 'overtime', 'total','fines','paid', 'dues', 'advance'],['user_id', '=', $user_id],$start_page,$parpage,'ORDER BY `date` DESC, `id` DESC');
	
	?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					
				<?php if($salleryHelper->getUserSalleryLimit($user_id, $start_page, $parpage))  : ?>
					<h4 class="block-title"><?php echo $employee->getEmployeeForSallery($user_id)->name ;?>'s Sallery Sheet (id=<?php echo $employee->getEmployeeForSallery($user_id)->emplolee_id;?>)</h4>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>	
								<th>No</th>	
								<th>Basic</th>	
								<th>Previous arrears</th>	
								<th>Allowance</th>	
								<th>Overtime</th>	
								<th>Fines</th>	
								<th>Total</th>
								<th>Paid</th>
								<th>Dues</th>
								<th>Advance</th>
								<th>Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $x =$pagination->getCurrentPageFirstItem(); foreach($salleryHelper->getUserSalleryLimit($user_id, $start_page, $parpage) as $sa) : ?>
							<tr>
								<td><?=$x;?></td>
								<td><?=$sa->basic;?></td>
								<td><?=$sa->previous_arrears;?></td>
								<td><?=$sa->allowance;?></td>
								<td><?=$sa->overtime;?></td>
								<td><?=$sa->fines;?></td>
								<td><?=$sa->total;?></td>
								<td><?=$sa->paid;?></td>
								<td><?=$sa->dues;?></td>
								<td><?=$sa->advance;?></td>
								<td><?=$sa->date;?></td>
								<td><a href="ajax/delete-item.php" id="<?=$sa->id;?>" data-table="<?php echo encryptMS('sallery_details');?>" onclick="delete_data(this); return false;">Delete</a></td>
							</tr>
						<?php $x++; endforeach;?>
							<tr>
								<td>Total</td>
								<td><?=$counting->basic;?></td>
								<td><?=$counting->previous_arrears;?></td>
								<td><?=$counting->allowance;?></td>
								<td><?=$counting->overtime;?></td>
								<td><?=$counting->fines;?></td>
								<td><?=$counting->total;?></td>
								<td><?=$counting->paid;?></td>
								<td><?=$counting->dues;?></td>
								<td><?=$counting->advance;?></td>
								<td>-</td>
								<td>Taka</td>
							</tr>
						</tbody>
					</table>
					</div>
				<?php 
					echo $pagination->toHTML();
					else :?>
					<h2>No data Found!</h2>
				<?php endif;?>
				</div>
			</div>
		</div>
		<script src="js/sweetalert.min.js"></script>
<?php require_once "includes/footer.php";?>