<?php
/**
* Sallery Helper Class . This is class will be helping Base Sallery Class
*/
class SalleryHelper extends Section {
	protected $table = 'sallery_details';

	public function getUserSallery($id){
		return $this->db->query("SELECT * FROM {$this->table} WHERE user_id = ? ORDER BY `id` DESC , `date` DESC",[$id])->results();
	}

	public function totalSallery($id){
		return $this->db->query("SELECT id FROM {$this->table} WHERE user_id = ?", [$id])->count();
	}

	public function getUserSalleryLimit($id, $start, $end){
		return $result = $this->db->query("SELECT * FROM {$this->table} WHERE user_id = ? ORDER BY `date` DESC, `id` DESC LIMIT {$start}, {$end}", [$id])->results();
	}

	

}