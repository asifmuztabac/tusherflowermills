<?php
	$title = "জমা খরচের হিসাব";
	require_once "includes/header.php";
	$account = new AccountName;
	$accc = new Account;
	?><div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<h4 class="block-title">Add Account Name</h4>
					<?php 
						$validator = new Validate(new ErrorHandler);
						if (!empty($_POST)) {
							$validator->check($_POST, [
								'account_name' => [
									'required' => true
								],
								'name' => [
									'required' => true,
									'maxlength' => 255
								],
								'details' => [
									'required' => true,

								]

							]);

							if($validator->passed()) {
								

								$add = $accc->create([
									'account_id' => $_POST['account_name'],
									'name' => $_POST['name'],
									'details' => $_POST['details']
								]);

								if ($add) {
									echo '<p class="alert alert-success fade in">New account name create successfully</p>';
								}


							}

						}
					?>
					<div class="row">
						<div class="col-md-6 col-md-offset-3 box-style">
							<form action="" method="post">
								<div class="form-group<?php echo $validator->errors()->first('account_name') != null ? ' has-error' : '';?>">
									<label for="account_name" class="control-label">Account name <span class="star">*</span></label>
									<select class="form-control" name="account_name" id="account_name">
										<option value="">Select option</option>
										<?php foreach($account->get() as $a) :?>
										<option value="<?=$a->id;?>"><?=$a->name;?></option>
										<?php endforeach;?>
									</select>
									<?php echo $validator->errors()->first('account_name') != null ? '<p class="help-block">'. $validator->errors()->first('account_name') .'</p>' : '';?>
								</div>

								<div class="form-group<?php echo $validator->errors()->first('name') != null ? ' has-error' : '';?>">
									<label for="name" class="control-label">Name <span class="star">*</span></label>
									<input type="text" name="name" class="form-control" id="name" placeholder="Enter Your name here">
									<?php echo $validator->errors()->first('name') != null ? '<p class="help-block">'. $validator->errors()->first('name') .'</p>' : '';?>
								</div>
								<div class="form-group<?php echo $validator->errors()->first('details') != null ? ' has-error' : '';?>">
									<label for="details" class="control-label">Details <span class="star">*</span></label>
									<textarea name="details" id="details" rows="5" placeholder="Enter data here" class="form-control"></textarea>
									<?php echo $validator->errors()->first('details') != null ? '<p class="help-block">'. $validator->errors()->first('details') .'</p>' : '';?>
								</div>
								<input type="submit" class="btn custom-btn" value="Submit">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
<?php require_once "includes/footer.php";?>