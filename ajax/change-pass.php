<?php
require_once '../core/inita.php';

if (!empty($_POST)) {
	
	$validator = new Validate(new ErrorHandler);
	$user = new User;
	$validator->check($_POST, [
			'old_password' => [
				'required' => true,
				'password_exists' => true

			],
			'new_password' => [
				'required' => true,
				'minlength' => 6
			],
			'new_password_again' => [
				'match' => 'new_password'
			]


		]);

	if($validator->passed()){
		if($user->changePassword($_POST['new_password'])){
			echo json_encode(['message' => "<p class='alert alert-success'>আপনার পাসওয়ার্ড সফল ভাবে পরিবর্তন করা হয়েছে</p>", 'valid' => true]);

		}
	}else{
		$arr = ['valid' => false];

		$final_array = array_merge($validator->errors()->allErrors(),$arr);

		//print_r($final_array);
		echo json_encode($final_array);
	}
}
