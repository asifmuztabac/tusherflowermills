function delete_data(that) {
	
	swal({  
		title: "নিশ্চিত হউন",
		text: "আপনি কি নিশ্চিত এই তথ্য মুছে ফেলবেন ?",
		type: "info",   showCancelButton: true,
		closeOnConfirm: false,
		showLoaderOnConfirm: true,
		allowOutsideClick : true},
		function(){
			var id = that.id,
				table = $(that).data('table'),
				link = that.href;
			$.ajax({
				url : link,
				data : {id : id, table : table},
				type : 'post',
				dataType : 'json',
				success : function(res){
					if(res.action === true){
						swal({title: "সফল!",
							text: "এই তথ্য এখন সফলভাবে মুছা হয়েছে",
							timer: 3000,
							showConfirmButton: true
						});

						$(that).parents('tr').remove();
					}else{
						swal({title: "ভুল!",
							text: "এই তথ্য আপনি মুছতে পারবেন না",
							timer: 3000,
							showConfirmButton: true,
							'type' : 'error'
						});
					}
				}
			});

		
	});


}

$('.left-menu-toggle').on('click', function(e){
	
	$('#ms-left-side').toggleClass('left-side-open');
	$(this).toggleClass('active');
	e.preventDefault();
});

