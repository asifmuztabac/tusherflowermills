<?php
class Input {

	public static function get($data) {
		if(isset($_POST[$data])){
			return $_POST[$data];
		}elseif(isset($_GET[$data])) {
			return $_GET[$data];
		}
		return '';
	}
}