<?php
require_once '../core/inita.php';
header("Content-type: text/javascript");
if(isset($_POST['id'])){
	$employee_id = $_POST['id'];
	$img = $_POST['image_name'];
	
	$employee = new Employee;
	
	if($employee->delete('id', $employee_id)){
	
		if(!empty($img)){
			unlink("../uploads/" . $img);
		}
	
		echo json_encode(['delete' => true]);

	}else{
		echo json_encode(['delete' => false]);
	}
	

}