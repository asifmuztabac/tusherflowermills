	<?php
		$title = "Edit Employee";
		ob_start();
		require_once "includes/header.php";
		if(isset($_GET['employee'])){
			$employee_id = $_GET['employee'];
		}else{
		 	Redirect::to('employees.php');
		}

		$employee = new Employee;

		?>
		<div class="right-side">
			<?php include_once "includes/sub-header.php";?>
			<div class="dash-content">
				<div class="dash-block box-style">
					<h4 class="block-title">Edit Employee</h4>
					<?php
						$validate = new Validate(new ErrorHandler);

						if(isset($_POST['submit'])){
							$validate->check($_POST, [
								'name' => [
									'required' => true,
									'minlength' => 3,
									'maxlength' => 50
								],
								'nid' => [
									'required' => true,
									'number' => true,
									'minlength' => 19
								],
								'mobile_number' => [
									'required' => true,
									'number' => true
								],
								'sallery' => [
									'required' => true,
									'number' => true
								],
								'joining_date' => [
									'required' => true
								],
								'designation' => [
									'required' => true
								],
								'present_address' => [
									'required' => true

								],
								'permanent_address' => [
									'required' => true
								]
							]);

							$validate->check($_FILES, [
									'image' => [
										'file_type' => 'jpeg,jpg,png,gif',
										'file_size' => 5
									]
								]);

							if(!empty($_FILES['image']['name'])) {
								$image_upload = true;
								$img_name = uniqid();

								if($user->uploadPhoto('image',$img_name)){
									$image_name = $user->getImageName('image', $img_name);
								}else{
									$image_upload = false;
								}

								if($employee->update([
										'name' => $_POST['name'],
										'nid' => $_POST['nid'],
										'father_name' => $_POST['father_name'],
										'mother_name' => $_POST['mother_name'],
										'mobile_number' => $_POST['mobile_number'],
										'joining_date' => $_POST['joining_date'],
										'designation' => $_POST['designation'],
										'sallery' => $_POST['sallery'],
										'image' => $image_name,
										'present_address' => $_POST['present_address'],
										'permanent_address' => $_POST['permanent_address']
									], ['id', '=', $employee_id]) && $image_upload){
									if(!empty($_POST['_img_name'])){
										unlink("uploads/{$_POST['_img_name']}");
									}
									

									echo '<p class="alert alert-success fade in">Data succssfuly Updated<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';

								}else{
									echo '<p class="alert alert-success fade in">Data not succssfuly Updated<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
								}
							}else{

								if ($employee->update([
										'name' => $_POST['name'],
										'nid' => $_POST['nid'],
										'father_name' => $_POST['father_name'],
										'mother_name' => $_POST['mother_name'],
										'mobile_number' => $_POST['mobile_number'],
										'joining_date' => $_POST['joining_date'],
										'designation' => $_POST['designation'],
										'sallery' => $_POST['sallery'],
										'present_address' => $_POST['present_address'],
										'permanent_address' => $_POST['permanent_address']
									], ['id','=', $employee_id])) {
									echo '<p class="alert alert-success fade in">Data succssfuly Updated<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
								}else{
									echo '<p class="alert alert-success fade in">Data not succssfuly Updated<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
								}
							}

							
						}

						$emp = $employee->getEmployee($employee_id);

					?>

					<?php if(!empty($emp)) :?>

					<form action="<?php self_action_q();?>" method="post" enctype="multipart/form-data">
						<div class="row">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('name') != null ? ' has-error' : '';?>">
								<label class="control-label" for="name">Name <span class="star">*</span></label>
									<input type="text" value="<?=$emp->name;?>" name="name" id="name" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('name') != null ? '<p class="help-block">'. $validate->errors()->first('name') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('nid') != null ? ' has-error' : '';?>">
								<label class="control-label" for="nid">NID Number <span class="star">*</span></label>
									<input type="text" value="<?=$emp->nid;?>" name="nid" id="nid" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('nid') != null ? '<p class="help-block">'. $validate->errors()->first('nid') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label" for="name">Father Name </label>
									<input type="text" value="<?=$emp->father_name;?>" name="father_name" id="name" class="form-control" placeholder="Enter Your Emploeey name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label" for="name">Mother Name</label>
									<input type="text" value="<?=$emp->mother_name;?>" name="mother_name" id="name" class="form-control" placeholder="Enter Your Emploeey name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('mobile_number') != null ? ' has-error' : '';?>">
								<label class="control-label" for="m_number">Mobile Number <span class="star">*</span></label>
									<input type="text" value="<?=$emp->mobile_number;?>" id="m_number" name="mobile_number" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('mobile_number') != null ? '<p class="help-block">'. $validate->errors()->first('mobile_number') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group<?php echo $validate->errors()->first('joining_date') != null ? ' has-error' : '';?>">
								<label class="control-label" for="joining_date">Joining date <span class="star">*</span></label>
									<input type="text" value="<?=$emp->joining_date;?>" id="joining_date" name="joining_date" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('joining_date') != null ? '<p class="help-block">'. $validate->errors()->first('joining_date') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group<?php echo $validate->errors()->first('designation') != null ? ' has-error' : '';?>">
								<label class="control-label" for="designation">Designation <span class="star">*</span></label>
									<input type="text" value="<?=$emp->designation;?>" id="designation" name="designation" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('designation') != null ? '<p class="help-block">'. $validate->errors()->first('designation') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('image') != null ? ' has-error' : '';?>">
								<label class="control-label" for="image">Image </label>
									<input type="file" id="image" name="image" class="form-control" placeholder="Enter Your Emploeey name">
									<?php echo $validate->errors()->first('image') != null ? '<p class="help-block">'. $validate->errors()->first('image') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								<label class="control-label" for="emplolee_id">Employee ID <span class="star">*</span></label>
									<input type="text" value="<?=$emp->emplolee_id;?>" readonly="" id="emplolee_id" class="form-control">
									
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group<?php echo $validate->errors()->first('sallery') != null ? ' has-error' : '';?>">
								<label class="control-label" for="sallery">Sallery <span class="star">*</span></label>
									<input type="text" value="<?=$emp->sallery;?>" name="sallery" id="sallery" class="form-control">
									<?php echo $validate->errors()->first('sallery') != null ? '<p class="help-block">'. $validate->errors()->first('sallery') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('present_address') != null ? ' has-error' : '';?>">
								<label class="control-label" for="present_address">Present Address <span class="star">*</span></label>
									<textarea type="text" id="present_address" rows="4" name="present_address" class="form-control" placeholder="Enter Your Emploeey name"><?=$emp->present_address;?></textarea>
									<?php echo $validate->errors()->first('present_address') != null ? '<p class="help-block">'. $validate->errors()->first('present_address') .'</p>' : '';?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group<?php echo $validate->errors()->first('permanent_address') != null ? ' has-error' : '';?>">
								<label class="control-label" for="permanent_address">Permanent Address <span class="star">*</span></label>
									<textarea type="text" id="permanent_address" rows="4" name="permanent_address" class="form-control" placeholder="Enter Your Emploeey name"><?=$emp->permanent_address;?></textarea>
									<?php echo $validate->errors()->first('permanent_address') != null ? '<p class="help-block">'. $validate->errors()->first('permanent_address') .'</p>' : '';?>
								</div>
							</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box-style">
								<img class="img-responsive" src="<?php echo empty($emp->image) ? "images/user.png" : "uploads/{$emp->image}";?>" alt="">
							</div>
						</div>
							
							<div class="col-md-12">
								<input name="submit" class="btn custom-btn" type="submit" value="Submit">
							</div>
						</div>
						<input type="hidden" value="<?=$emp->image;?>" name="_img_name">
					</form>

				<?php else : ?>
				<h2>There are no employee no Found!</h2>
				<?php endif;?>
				</div>
			</div>
			<script src="js/bootstrap-datepicker.js"></script>
			<script>
				
				$('#joining_date').datepicker({
					format : 'yyyy-mm-dd'
				});
			</script>
		</div>
<?php require_once "includes/footer.php";?>