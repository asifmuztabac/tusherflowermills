<?php
$title = "বাকী রেজিস্টার";
require_once "includes/header.php";
$stock = new Stock;
$category = new Category;
$session= new Session;
$sell= new Sale;
?><div class="right-side">
<?php include_once "includes/sub-header.php";?>
<div class="dash-content">
	<div class="dash-block box-style">
		<div class="block-title">
			<a href="add-sell.php" class="btn custom-btn">New Sell</a>
			<a href="" class="btn custom-btn" id="showmonthstock">Show Monthly</a>
			<a href="" class="btn custom-btn" id="showyearstock">Show Yearly</a>
			<a href="check-dues.php" class="btn custom-btn" id="showyearstock">Check Out Dues</a>
			<br>
			<div class="selectMonth">
				<div class="form-group" id="month_date_div" style="display: none;">
					<label for="month_date">Select Month:</label>
					<input type="text" class="form-control" name="month_date" id="month_date_selector">
				</div>
			</div>

			<div class="selectMonth">
				<div class="form-group" id="year_date_div" style="display: none;">
					<label for="year_date">Select Year:</label>
					<input type="text" class="form-control" name="year_date" id="year_date_selector">
				</div>
			</div>
		</div>
		<div class="block-title-sub">

			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<p>The List is Showing Todays Due List</p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<p align="right">Select Another Date: </p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<input type="text" value="<?php echo date('Y-m-d');?>" class="form-control" id="adding_date" name="date">
				</div>

			</div>

		</div>
		<div class="table-responsive" id="dataTable">
			<?php


			if(empty($session->exists('date'))){
				$date=date('Y-m-d');
				$dataDate=$sell->allDataDateDue($date);
			}
			else{
				$date=$session->get('date');
				if(strlen($date)>=10){
					$dataDate=$sell->allDataDateDue($date);
							//var_dump($dataDate);
				}
				else if(strlen($date)==7){
					$dateexplode=explode('-', $date);
					$month=$dateexplode['1'];
					$year=$dateexplode['0'];
					$dataMonth=$sell->allMonthDataSaleDue($month,$year);
					//var_dump($dataMonth);
				}
				else if(strlen($date)==4){
							//var_dump($date);
					$dataYear=$sell->allYearDataSaleDue($date);
							//var_dump($dataYear);
				}
				else{

				}
			}
			$totalPaidAmount='';
			$totalDueAmount='';
			$totalCostAmount='';
			?>
			<?php if(isset($dataDate)){if(!empty($dataDate)) : ?>
				<table class="table table-striped table-bordered table-hover" style="text-align:center">
					<thead>
						<tr>
							<th rowspan="2" style="text-align:center;width:10%">ক্রমিক নং</th>		
							<th rowspan="2" style="text-align:center;width:10%">ক্রেতার নাম</th>	
							<th rowspan="2" style="text-align:center;width:10%">মোট খরচ</th>	
							<th colspan="0" style="text-align:center;width:20%">মোট জমা</th>
							<th colspan="0" style="text-align:center;width:20%">মোট বাকী</th>
							<th rowspan="2" style="text-align:center;width:10%">বিস্তারিত</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($dataDate as $datam) {?>
						<tr>
							<td><?=$datam->id;?></td>
							<td><?=$datam->customer_name;?></td>
							<td><?=$datam->total_cost; ?></td>
							<td><?=$datam->total_paid;?></td>
							<td><?=$datam->total_due;?></td>
							<td><a href="sale-payment.php">Payment</a></td>
						</tr>

						<?php 
						$totalCostAmount=$datam->total_cost+$totalCostAmount;
						$totalPaidAmount=$datam->total_paid+$totalPaidAmount;
						$totalDueAmount=$datam->total_due+$totalDueAmount;
					}?>
					<tr>
						<td colspan="2" style="text-align:center;width:10%">মোট</td>
						<td><?=$totalCostAmount;?></td>
						<td><?=$totalPaidAmount;?></td>
						<td><?=$totalDueAmount;?></td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php else : ?>
		<h2>No data Found!</h2>
		<?php 
		endif;
	}
	?>
	<?php if(isset($dataMonth)){if(!empty($dataMonth)) : ?>
		<table class="table table-striped table-bordered table-hover" style="text-align:center">
			<thead>
				<tr>
					<th rowspan="2" style="text-align:center;width:10%">ক্রমিক নং</th>		
					<th rowspan="2" style="text-align:center;width:10%">ক্রেতার নাম</th>	
					<th rowspan="2" style="text-align:center;width:10%">মোট খরচ</th>	
					<th colspan="0" style="text-align:center;width:20%">মোট জমা</th>
					<th colspan="0" style="text-align:center;width:20%">মোট বাকী</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($dataMonth as $datam) {?>
				<tr>
					<td><?=$datam->id;?></td>
					<td><?=$datam->customer_name;?></td>
					<td><?=$datam->total_cost; ?></td>
					<td><?=$datam->total_paid;?></td>
					<td><?=$datam->total_due;?></td>
				</tr>

						<?php 
						$totalCostAmount=$datam->total_cost+$totalCostAmount;
						$totalPaidAmount=$datam->total_paid+$totalPaidAmount;
						$totalDueAmount=$datam->total_due+$totalDueAmount;
					}?>
					<tr>
						<td colspan="2" style="text-align:center;width:10%">মোট</td>
						<td><?=$totalCostAmount;?></td>
						<td><?=$totalPaidAmount;?></td>
						<td><?=$totalDueAmount;?></td>
					</tr>
		</tbody>
	</table>
</div>
<?php else : ?>
	<h2>No data Found!</h2>
	<?php 
	endif;
}
?>
<?php if(isset($dataYear)){if(!empty($dataYear)) : ?>
	<table class="table table-striped table-bordered table-hover" style="text-align:center">
		<thead>
			<tr>
				<th rowspan="2" style="text-align:center;width:10%">ক্রমিক নং</th>		
				<th rowspan="2" style="text-align:center;width:10%">ক্রেতার নাম</th>	
				<th rowspan="2" style="text-align:center;width:10%">মোট খরচ</th>	
				<th colspan="0" style="text-align:center;width:20%">মোট জমা</th>
				<th colspan="0" style="text-align:center;width:20%">মোট বাকী</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dataYear as $datam) {?>
			<tr>
				<td><?=$datam->id;?></td>
				<td><?=$datam->customer_name;?></td>
				<td><?=$datam->total_cost; ?></td>
				<td><?=$datam->total_paid;?></td>
				<td><?=$datam->total_due;?></td>
			</tr>

						<?php 
						$totalCostAmount=$datam->total_cost+$totalCostAmount;
						$totalPaidAmount=$datam->total_paid+$totalPaidAmount;
						$totalDueAmount=$datam->total_due+$totalDueAmount;
					}?>
					<tr>
						<td colspan="2" style="text-align:center;width:10%">মোট</td>
						<td><?=$totalCostAmount;?></td>
						<td><?=$totalPaidAmount;?></td>
						<td><?=$totalDueAmount;?></td>
					</tr>
	</tbody>
</table>
</div>
<?php else : ?>
	<h2>No data Found!</h2>
	<?php 
	endif;
}
?>

</div>
</div>
</div>
<script src="js/sweetalert.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script>
	$('#adding_date').datepicker({
		format : 'yyyy-mm-dd'
	});
	$('#adding_date').on('changeDate',function(){
		var dval=$('#adding_date').val();
		$.ajax({
			url : 'ajax/get-stock.php',
			type : 'post',
			dataType : 'json',
			data : {
				date : dval
			},
			success: function(data){
				console.log(data);
				//$("#dataTable").load().fadeIn('fast');
				$("#dataTable").load(document.URL +  ' #dataTable');
			}
		});
	});
	$('#month_date_selector').datepicker({
		format : 'yyyy-mm',
		viewMode: "months", 
		minViewMode: "months"
	});
	$('#year_date_selector').datepicker({
		format : 'yyyy',
		viewMode: "years", 
		minViewMode: "years"
	});
	$('#showmonthstock').on('click',function(){
		$('#year_date_div').slideUp();
		$('#month_date_div').slideToggle();
		return false;
	});
	$('#showyearstock').on('click',function(){
		$('#month_date_div').slideUp();
		$('#year_date_div').slideToggle();
		return false;
	});
	$('#year_date_selector').on('changeYear',function(){
		var yval=$('#year_date_selector').val();
		$.ajax({
			url : 'ajax/get-stock.php',
			type : 'post',
			dataType : 'json',
			data : {
				date : yval
			},
			success: function(data){
				console.log(data);
					        			//$("#dataTable").load().fadeIn('fast');
					        			$("#dataTable").load(document.URL +  ' #dataTable');
					        		}
					        	});
	});
	$('#month_date_selector').on('changeMonth',function(){
		var mval=$('#month_date_selector').val();
		$.ajax({
			url : 'ajax/get-stock.php',
			type : 'post',

			data : {
				date : mval
			},
			success: function(data){
				console.log(data);
				$("#tableDate").remove();
				$("#dataTable").load(document.URL +  ' #dataTable');
			}
		});
	});
</script>
<?php $session->delete('date'); ?>		
<?php require_once "includes/footer.php";?>