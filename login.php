<?php

require_once 'core/init.php';

$validate = new Validate(new ErrorHandler);
$user = new User;
if($user->isLogin()){
	Redirect::to('index.php');
}
if (!empty($_POST)) {
	 $validate->check($_POST, [
			'username' => [
				'required' => true
			],
			'password' => [
				'required' => true
			]
		]);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login Your Account</title>
	<link rel="stylesheet" href="style.css">
</head>
<body class="login-body">
	<div class="container">
		<div class="login-form">
				<?php
					$forgot = false;
					if(isset($_GET['info'])){
						switch ($_GET['info']) {
							case 'logout':
								echo '<div class="alert alert-info" role="alert">You are now logged out. </div>';
								break;
							
							case 'error':
								echo '<div class="alert alert-danger" role="alert">Please login your account. </div>';
								break;
							case 'forgot':
								echo '<div class="alert alert-info" role="alert">Please enter your Registered Email. </div>';
								$forgot = true;
								break;
						}
					}else{
						echo '<div class="alert alert-info" role="alert"> Please Login Your Account ! </div>';
					}
					

					if(!empty($_POST)){
						if(!$validate->errors()->hasErrors()){
							try {
								$user->login(Input::get('username'), Input::get('password'));
							} catch (Exception $e) {
								echo "<p class='alert alert-danger'>{$e->getMessage()}</p>";
							}
				 		
				 		}
					}
					
			 	?>
		<?php if(!$forgot) :?>
			<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
				<div class="form-group<?php echo ($validate->errors()->first('username')) != null ? ' has-error' : '';?>">
					<label class="control-label" for="username">Username <span class="star">*</span></label>
					<input type="text" class="form-control" value="<?php echo Input::get('username');?>" placeholder="Enter your Username..." id="username" name="username">
					<?php echo ($validate->errors()->first('username')) != null ? '<p class="help-block">' . $validate->errors()->first('username') . '</p>' : '';?>
				</div>
				<div class="form-group<?php echo ($validate->errors()->first('password')) != null ? ' has-error' : '';?>">
					<label class="control-label" for="password">Password <span class="star">*</span></label>
					<input type="password" class="form-control" placeholder="Enter your Password..." id="password" name="password">
					<?php echo ($validate->errors()->first('password')) != null ? '<p class="help-block">' . $validate->errors()->first('password') . '</p>' : '';?>
				</div>
				<button type="submit" class="btn btn-success">Login</button>
				<a href="?info=forgot" class="btn btn-warning" style="float: right;">Forgot Password</a>
			</form>
		<?php else :?>
			<form action="ajax/forgot-password.php" method="post">
				<div class="form-group">
					<label class="control-label" for="email">Email <span class="star">*</span></label>
					<input type="email" required="true" class="form-control" placeholder="Enter your email..." id="email" name="email">
					
				</div>
				<button type="submit" class="btn btn-success">Submit</button>
				<a href="<?=self_action();?>" class="btn btn-warning" style="float: right;">Back to login</a>
			</form>
		<?php endif;?>
		</div>
	</div>
</body>
</html>