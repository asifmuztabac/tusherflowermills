<?php
require_once '../core/inita.php';
if(isset($_POST['id'])) {
	$table = decryptMS($_POST['table']);
	$id = $_POST['id'];

	$db = Database::connect();
	$delete = $db->delete($table, 'id', $id);
	if($delete){
		echo json_encode(['action' => true]);
	}else{
		echo json_encode(['action' => false]);
	}

}