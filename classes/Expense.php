<?php
/**
* This class is Truck Rent and Others.
*/
class Expense extends Section
{
	protected $table = 'expense';
	
	public function getEmployee($id){
		return $this->db->query("SELECT * FROM {$this->table} WHERE id = ?", [$id])->first();
	}
	
	/*public function ExpenseReprt($start, $end){
		return $result = $this->db->query("SELECT * FROM {$this->table} WHERE OrderDate BETWEEN {$start} AND {$end}";
	}*/
	public function getExpense($id,$sub_id,$start,$end)
	{
		return $this->db->query("SELECT id,vouchar_no,amount,paid,due,date,details FROM {$this->table} WHERE account_id = ? AND sub_account_id = ?  ORDER BY id DESC LIMIT {$start},{$end}", [$id,$sub_id])->results();
	}

	public function getExpenseCount($id,$id2)
	{
		return $this->db->query("SELECT COUNT(*) AS total FROM {$this->table} WHERE account_id=? AND sub_account_id=?", [$id,$id2])->first();
	}

	public function getExpenseByDate($id,$sub_id,$start,$end)
	{
		return $this->db->query("SELECT id,vouchar_no,amount,paid,due,date,details FROM {$this->table} WHERE account_id = ? AND sub_account_id = ? AND date BETWEEN ? AND ? ORDER BY id DESC", [$id,$sub_id,$start,$end])->results();
	}


	
}