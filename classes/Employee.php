<?php
/**
* Employee Class
*/
class Employee extends Section {
	
	protected $table = 'employees';


	public function get(){
		return $this->db->pdo()->query("SELECT id,name,nid,mobile_number,image,sallery,designation,emplolee_id FROM {$this->table}")->fetchAll(PDO::FETCH_OBJ);
	}

	public function getEmployee($id){
		return $this->db->query("SELECT * FROM {$this->table} WHERE id = ?", [$id])->first();
	}

	public function getEmployeeForSallery($id){
		return $this->db->query("SELECT id,emplolee_id,name FROM {$this->table} WHERE id = ?", [$id])->first();
	}
}